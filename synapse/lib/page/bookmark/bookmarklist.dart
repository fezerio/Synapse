import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/bookmark.dart';
import 'package:synapse/model/bookmarkData.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/question.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:synapse/widget/chapterWidget.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';
import '../../config/constant.dart';

class BookMarkList extends StatefulWidget {
  @override
  _BookMarkListState createState() => _BookMarkListState();
}

class _BookMarkListState extends State<BookMarkList> {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  Map<String, List<BookMarkData>> bookmarkData;

  Map<String, List<BookMarkData>> groupDataByDate(List<BookMarkData> chapter) {
    return chapter.groupBy((e) => e.bookmarks.created_date);
  }

  fetchData() async {
    final data = await BookmarkService().getBookMarks();

    bookmarkData = groupDataByDate(data);

    refreshController.refreshCompleted();
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SynapseAppBar(
        title: 'Bookmark',
      ),
      body: bookmarkData == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : bookmarkData.isEmpty
              ? emptyDataWidget()
              : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 22),
                        child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final chapters =
                                bookmarkData.values.toList()[index];

                            return Padding(
                              padding: const EdgeInsets.only(bottom: 16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    DateFormat('dd MMM yyyy').format(
                                      DateTime.parse(bookmarkData.keys
                                          .toList()[index]
                                          .toString()),
                                    ),
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.7),
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: chapters.length,
                                    itemBuilder: (context, index) => Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 9),
                                      child: BookmarkWidget(
                                        bookmark: chapters[index],
                                        onBookmarkRemoval: () {
                                          fetchData();
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          itemCount: bookmarkData.length,
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }
}

class BookmarkWidget extends StatelessWidget {
  final BookMarkData bookmark;
  // final String title;
  final Function onBookmarkRemoval;
  const BookmarkWidget({
    Key key,
    this.bookmark,
    this.onBookmarkRemoval,
    // this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(CupertinoPageRoute(
          builder: (context) => BookmarkQuestion(
            question: bookmark.question,
            title: bookmark.question.chapter_name ?? '',
          ),
        ))
            .then((value) {
          if (value ?? false) onBookmarkRemoval();
        });
      },
      child: Container(
        padding: const EdgeInsets.all(22),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  // spreadRadius: 0,
                  offset: Offset(0, 6))
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Text(
                bookmark.question.question,
                maxLines: 3,
                overflow: TextOverflow.clip,
                style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BookmarkQuestion extends StatefulWidget {
  final Question question;
  final String title;
  BookmarkQuestion(
      {Key key,
      this.question,
      //   this.isSubmitted = false,
      this.title})
      : super(key: key);
  @override
  _BookmarkQuestionState createState() => _BookmarkQuestionState();
}

class _BookmarkQuestionState extends State<BookmarkQuestion> {
  int rightAnswer = 0;
  int totalAnswer = 0;
  @override
  void initState() {
    super.initState();
    QuestionService()
        .getQuestionRightAnswerInfo(widget.question.id)
        .then((value) {
      rightAnswer = value['rightAnswer'];
      totalAnswer = value['totalAnswer'];
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool isReported = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SynapseAppBar(
        title: widget.title,
        elevation: 5,
        actions: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 8),
              child: Tooltip(
                message: 'Remove Bookmark',
                child: InkWell(
                    onTap: () {
                      BookmarkService().removeBookMark(widget.question.id);
                      Navigator.of(context).pop(true);
                      Toast.show('Question Removed from bookmark', context);
                    },
                    child: Icon(MdiIcons.bookmarkRemove)),
              ),
            ),
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 37, right: 30),
            child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  ListTile(
                    title: Text(
                      widget.question?.question.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    contentPadding: const EdgeInsets.all(0),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  listWidget(
                      index: 0, option: widget.question?.option_a, title: 'a'),
                  listWidget(
                      index: 1, option: widget.question?.option_b, title: 'b'),
                  listWidget(
                      index: 2, option: widget.question?.option_c, title: 'c'),
                  listWidget(
                      index: 3, option: widget.question?.option_d, title: 'd'),
                ]),
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 37, right: 37),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    displayExplanation(context, widget.question.explanation,
                        correctAnswer: rightAnswer, totalAnswer: totalAnswer);
                  },
                  child: Text(
                    'See Explanation',
                    style: TextStyle(
                        color: appColor,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid),
                  ),
                ),
                InkWell(
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    if (!isReported) {
                      isReported = true;
                      Toast.show('Question has been reported', context);
                      BookmarkService().reportQuestion(widget.question.id);
                    }
                  },
                  child: Text(
                    'Report',
                    style: TextStyle(
                        color: appColor,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listWidget({String title, int index, String option}) {
    final boxColor =
        index + 1 == widget.question.answer ? Colors.green : Colors.white;
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: boxColor,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  // spreadRadius: 0,
                  offset: Offset(0, 6))
            ]),
        child: Padding(
          padding: const EdgeInsets.only(left: 25, right: 15),
          child: ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
            title: Text(
              option.toString(),
              style: TextStyle(
                  color:
                      boxColor != Colors.white ? Colors.white : Colors.black),
            ),
          ),
        ),
      ),
    );
  }
}
