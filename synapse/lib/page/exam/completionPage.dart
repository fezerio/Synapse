import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/page/exam/resultPage.dart';
import 'package:synapse/page/mainPage.dart';
import 'package:synapse/widget/synapseappbar.dart';

class ExamCompletionPage extends StatelessWidget {
  final int chapterId;
  final String chapterName;
  final int timer;
  final bool isMocktest;

  const ExamCompletionPage(
      {Key key,
      this.chapterId,
      this.chapterName,
      this.timer,
      this.isMocktest = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // appBar: SynapseAppBar(
      //   title: 'Synapse',
      //   isExamPage: true,
      //   centetTile: true,
      // ),
      body: SafeArea(
        child: Padding(
          padding:
              EdgeInsets.only(bottom: ScreenSizeConfig.blockSizeVertical * 8),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: ScreenSizeConfig.safeBlockVertical * 5,
                  ),
                  Text(
                    'HURRAY!',
                    style: TextStyle(
                        fontSize: 25,
                        letterSpacing: 1,
                        color: appColor,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "You've completed the test.",
                    style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1,
                        color: appColor,
                        fontWeight: FontWeight.bold),
                  ),
                  Expanded(
                      child: FlareActor(
                    'assets/flare/celebration.flr',
                    alignment: Alignment.center,
                    animation: 'relig',
                    isPaused: false,
                  )),
                ],
              )),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: InkWell(
                        focusColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          color: Colors.transparent,
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.arrow_back,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Flexible(
                                child: Text(
                                  'Back To Menu',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40),
                        child: InkWell(
                          onTap: () {
                            // return;
                            Navigator.pushReplacement(
                                context,
                                CupertinoPageRoute(
                                  builder: (context) => ResultPage(
                                    chapterId: chapterId,
                                    chapterName: chapterName,
                                    isMockTest: isMocktest,
                                    timer: timer,
                                  ),
                                ));
                          },
                          child: Container(
                            color: appColor,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Center(
                                    child: Text(
                                      'See Result',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
