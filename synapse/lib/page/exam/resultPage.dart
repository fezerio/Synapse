import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/result.dart';
import 'package:synapse/model/resultAnswer.dart';
import 'package:synapse/page/exam/examHomePage.dart';
import 'package:synapse/page/exam/resultAnswers.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/service/resultservice.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';

class ResultPage extends StatelessWidget {
  final int chapterId;
  final String chapterName;
  final bool isMockTest;
  final int timer;

  const ResultPage(
      {Key key,
      this.chapterId,
      this.chapterName,
      this.isMockTest = false,
      @required this.timer})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SynapseAppBar(
        title: chapterName,
      ),
      // backgroundColor: Colors.white,
      body: Center(
        child: FutureBuilder<Result>(
            future: ResultService().getChapterResult(chapterId),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  final score = ((snapshot.data.attempted_question +
                              snapshot.data.left_question) ==
                          0)
                      ? 0
                      : ((snapshot.data.correct_answer) /
                              (snapshot.data.attempted_question +
                                  snapshot.data.left_question)) *
                          100;

                  return Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                      width: ScreenSizeConfig.blockSizeHorizontal * 80,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 25),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            score > 50 ? 'Congratulations' : 'Sorry!!!',
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 25,
                                color: score > 50 ? Colors.green : Colors.red),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          score > 50
                              ? Image.asset('assets/images/success.png')
                              : Image.asset('assets/images/failure.png'),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            (score ?? 0).toInt().toString() + '% Score',
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 25,
                                color: score > 50 ? Colors.green : Colors.red),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                              children: [
                                TextSpan(text: 'You attempted '),
                                TextSpan(
                                  text: snapshot.data.attempted_question
                                      .toString(),
                                  style: TextStyle(
                                      color: Colors.yellow[900],
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                ),
                                TextSpan(text: ' out of '),
                                TextSpan(
                                  text: (snapshot.data.attempted_question +
                                              snapshot.data.left_question)
                                          .toString() +
                                      ' questions ',
                                  style: TextStyle(
                                      color: Colors.blue[900],
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                ),
                                TextSpan(text: ' from which '),
                                TextSpan(
                                  text: (snapshot.data.correct_answer)
                                          .toString() +
                                      ' answers ',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                ),
                                TextSpan(text: ' is correct. '),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              InkWell(
                                focusColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (context) => AlertDialog(
                                      title: Text(
                                        'Re-take this test?',
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      content: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Text(
                                              'If u agree, previous result will be deleted. This process is not reversible.',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            RaisedButton(
                                              color: Colors.green,
                                              onPressed: () async {
                                                ProgressDialog pr =
                                                    ProgressDialog(
                                                  context,
                                                  isDismissible: false,
                                                );
                                                pr.style(
                                                    message:
                                                        'Deleting previous result information');
                                                await pr.show();
                                                bool cc = await ChapterService()
                                                    .deleteTest(chapterId);
                                                await pr.hide();
                                                Navigator.pop(context);
                                                if (cc) {
                                                  Navigator.of(context)
                                                      .pushReplacement(
                                                          CupertinoPageRoute(
                                                    builder: (context) =>
                                                        ExamPage(
                                                      isMockTest: isMockTest,
                                                      chapterId: chapterId,
                                                      chapterName: chapterName,
                                                      time: timer,
                                                    ),
                                                  ));
                                                } else
                                                  Toast.show(
                                                      'Error deleting previous information, Please try again!',
                                                      context,
                                                      duration: 3);
                                              },
                                              child: Text(
                                                'Agree',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                child: Text(
                                  'Re-take Test',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decorationStyle:
                                          TextDecorationStyle.double,
                                      decoration: TextDecoration.underline,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                ),
                              ),
                              InkWell(
                                focusColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                        builder: (context) => ResultAnswersPage(
                                          chapterId: chapterId,
                                          title: chapterName,
                                        ),
                                      ));
                                },
                                child: Text(
                                  'Reveal Answers',
                                  style: TextStyle(
                                      color: Colors.blue,
                                      decorationStyle:
                                          TextDecorationStyle.double,
                                      decoration: TextDecoration.underline,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                } else
                  return Text(
                    'Error Generating Result',
                    style: emptyListStyle,
                  );
              }
              return CircularProgressIndicator(
                backgroundColor: Colors.white,
              );
            }),
      ),
    );
  }
}
