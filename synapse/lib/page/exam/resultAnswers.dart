import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/model/resultAnswer.dart';
import 'package:synapse/service/resultservice.dart';
import 'package:synapse/widget/resultQuestions.dart';
import 'package:synapse/widget/synapseappbar.dart';

class ResultAnswersPage extends StatelessWidget {
  final int chapterId;
  final String title;
  ResultAnswersPage({Key key, this.title, this.chapterId}) : super(key: key);
  PageController pageController = PageController(
    initialPage: 0,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SynapseAppBar(
        title: title ?? 'Result Answers',
      ),
      body: FutureBuilder<List<ResultAnswer>>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty) {
              return Text('Empty Questiions');
            } else
              return PageView.builder(
                controller: pageController,
                physics: NeverScrollableScrollPhysics(),
                itemCount: snapshot.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => ResultQuestionWidget(
                  isLastPage: index == (snapshot.data.length - 1),
                  firstPage: index == 0,
                  onNextPress: () {
                    pageController.nextPage(
                        duration: Duration(milliseconds: 800),
                        curve: Curves.easeOutExpo);
                  },
                  onSubmit: () {},
                  onPreviousPress: () {
                    pageController.previousPage(
                        duration: Duration(milliseconds: 800),
                        curve: Curves.easeOutExpo);
                  },
                  question: snapshot.data[index],
                ),
              );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<List<ResultAnswer>> getData() async {
    return await ResultService().getResultAnswers(chapterId);
    ;
  }
}
