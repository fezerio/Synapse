import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/errorHandler.dart';
import 'package:synapse/model/question.dart';
import 'package:synapse/page/exam/completionPage.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:synapse/widget/countdownTimer.dart';
import 'package:synapse/widget/interactiveQuestion.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';

class ExamPage extends StatefulWidget {
  final String title;
  final int time;
  final String chapterName;
  final int chapterId;
  final bool isMockTest;
  const ExamPage({
    Key key,
    this.chapterName = '',
    this.title,
    this.time,
    this.chapterId,
    this.isMockTest = false,
  }) : super(key: key);
  @override
  _ExamPageState createState() => _ExamPageState();
}

class _ExamPageState extends State<ExamPage> with WidgetsBindingObserver {
  // int totalQuestions = 0;
  // int rightAnswers = 0;
  // int wrongAnswers = 0;
  // StreamController streamController = StreamController();
  List<Question> questions;
  //int temp = 0;
  bool examEnded = false;
  // Timer timer;
  // int countDown;
  // AppLifecycleState lifecycleState;
  @override
  void initState() {
    super.initState();
    //  countDown = widget.time;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(seconds: 1)).then((value) {
        fetchQuestions().then((value) {
          questions = value;
          if (this.mounted) {
            setState(() {});
          }
        });
      });

      // if (widget.isSubjectTest) {
      //   timer = Timer.periodic(Duration(seconds: 1), (timer) {
      //     print('ds');
      //     countDown--;
      //     streamController.add(countDown);
      //     if (countDown == 0 || countDown < 0) {
      //       timer.cancel();

      //       streamController.add(0);
      //       streamController.close();

      //       _haxExamEnded.value = true;
      //     }
      //   });
      // }
    });

    //  WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    // streamController?.close();
    _haxExamEnded?.removeListener(() {});
    _haxExamEnded.dispose();
    // timer?.cancel();
    // streamController?.close();
    // WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  ValueNotifier<bool> _haxExamEnded = ValueNotifier(false);
  PageController pageController = PageController();

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   if (state.index == 2) {
  //     print('dsaasdsad');
  //     print(countDown);
  //     setState(() {});
  //     {
  //       temp = countDown;
  //     }
  //   }
  //   if (state.index == 0) {
  //     print(temp);
  //     setState(() {});
  //     {
  //       countDown = temp ?? countDown;
  //     }
  //     print(countDown);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: SynapseAppBar(
          title: '',
          centetTile: false,
          isExamPage: true,
          actions: <Widget>[
            if (questions != null)
              if (questions.isNotEmpty)
                if (widget.time != 0 && widget.isMockTest)
                  Center(
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: TimerWidget(
                            isMCQ: false,
                            isExamTimer: true,
                            duration: widget.time,
                            onCountDownFinish: () {
                              if (!examEnded) _haxExamEnded.value = true;
                            })),
                  ),
            SizedBox(
              width: 4,
            ),
            if (questions != null)
              if (questions.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Center(
                    child: InkWell(
                        onTap: () {
                          Toast.show('Question has been Bookmarked', context);
                          BookmarkService().insertBookmark(
                              questions[pageController.page.toInt()].id);
                        },
                        child: Icon(Icons.bookmark_border)),
                  ),
                )
          ],
        ),
        body: questions == null
            ? buildLoadingWidget()
            : questions.isEmpty
                ? buildEmptyWidget()
                : PageView.builder(
                    controller: pageController,
                    itemCount: questions.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return ValueListenableBuilder<bool>(
                          valueListenable: _haxExamEnded,
                          builder: (context, value, child) {
                            return Scaffold(
                              // floatingActionButtonLocation: ((index ==
                              //             (questions.length - 1)) ||
                              //         _haxExamEnded.value)
                              //     ? FloatingActionButtonLocation.endDocked
                              //     : FloatingActionButtonLocation.centerDocked,
                              // floatingActionButton: Padding(
                              //   padding: const EdgeInsets.only(bottom: 20),
                              //   child: FloatingActionButton(
                              //     backgroundColor: Colors.red,
                              //     onPressed: () async {
                              //       // bool callback = await DatabaseService()
                              //       //     .checkBucket(questions[index].id);
                              //       // if (callback) {
                              //       //   Toast.show(
                              //       //       'Question has already been reported',
                              //       //       context,
                              //       //       duration: 4);
                              //       //   return;
                              //       // }
                              //       DatabaseService()
                              //           .addBugBucket(BugBucket(
                              //               chapter_id:
                              //                   questions[index].chapter_id,
                              //               question_id: questions[index].id,
                              //               explanation:
                              //                   questions[index].explanation,
                              //               optionA: questions[index].optionA,
                              //               optionB: questions[index].optionB,
                              //               optionC: questions[index].optionC,
                              //               optionD: questions[index].optionD,
                              //               question:
                              //                   questions[index].question))
                              //           .then((value) {
                              //         Toast.show(
                              //             'Question has been added to bug bucket list',
                              //             context,
                              //             duration: 4);
                              //       });
                              //     },
                              //     child: Icon(
                              //       Icons.report,
                              //       size: 35,
                              //     ),
                              //   ),
                              // ),
                              backgroundColor: Colors.white,
                              body: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 20),
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 35, right: 30),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            RichText(
                                                text: TextSpan(children: [
                                              TextSpan(
                                                  text: 'Question ${index + 1}',
                                                  style: TextStyle(
                                                      fontSize: 22,
                                                      color: Colors.black
                                                          .withOpacity(0.7),
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              TextSpan(
                                                  text: '/${questions.length}',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 17))
                                            ])),
                                            Divider(
                                              thickness: 1.5,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      InteractiveQuestion(
                                        isMockTest: widget.isMockTest,
                                        isLastPage:
                                            (index == (questions.length - 1)) ||
                                                _haxExamEnded.value,
                                        // isSubmitted: _haxExamEnded.value,
                                        isTest: true,
                                        onNextPress: (isTrue, userAanswer) {
                                          try {
                                            QuestionService().questionSolved(
                                                questionId: questions[index].id,
                                                chapterId:
                                                    questions[index].chapter_id,
                                                correctAnswer:
                                                    questions[index].answer,
                                                userAnswer: userAanswer);
                                          } on ErrorHandler catch (e) {
                                            Toast.show(e.message, context);
                                          }
                                          pageController.nextPage(
                                              duration:
                                                  Duration(milliseconds: 800),
                                              curve: Curves.easeOutExpo);
                                        },
                                        onSubmit: (isTrue, userAnswer) async {
                                          if (!examEnded) {
                                            setState(() {
                                              examEnded = true;
                                            });
                                            ProgressDialog dialog =
                                                ProgressDialog(context,
                                                    isDismissible: false,
                                                    type: ProgressDialogType
                                                        .Normal);
                                            dialog.style(
                                                message: "Submitting Result");
                                            if (_haxExamEnded.value ?? false) {
                                              //exam is finished because time has ended

                                              final v = questions
                                                  .skip(index)
                                                  .toList();
                                              for (int i = 0;
                                                  i < v.length;
                                                  i++) {
                                                await QuestionService()
                                                    .questionSolved(
                                                        questionId: v[i].id,
                                                        chapterId:
                                                            v[i].chapter_id,
                                                        correctAnswer:
                                                            v[i].answer,
                                                        userAnswer: null);
                                              }
                                            } else {
                                              await QuestionService()
                                                  .questionSolved(
                                                      questionId:
                                                          questions[index].id,
                                                      chapterId:
                                                          questions[index]
                                                              .chapter_id,
                                                      correctAnswer:
                                                          questions[index]
                                                              .answer,
                                                      userAnswer: userAnswer);
                                            }
                                            await dialog.hide();
                                            Navigator.of(context)
                                                .pushReplacement(
                                                    CupertinoPageRoute(
                                              builder: (context) =>
                                                  ExamCompletionPage(
                                                      chapterId:
                                                          widget.chapterId,
                                                      isMocktest:
                                                          widget.isMockTest,
                                                      timer: widget.time,
                                                      chapterName:
                                                          widget.chapterName),
                                            ));
                                          }
                                        },
                                        question: questions[index],
                                        timeEnded: _haxExamEnded.value,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    },
                  ));
  }

  // Widget localTimerWidget() {
  //   return StreamBuilder(
  //     stream: streamController.stream,
  //     builder: (context, snapshot) {
  //       print('d1s');
  //       return AnimatedDefaultTextStyle(
  //         duration: Duration(seconds: 2),
  //         style: TextStyle(
  //             color: getColor(snapshot?.data ?? widget.time),
  //             fontSize: 22,
  //             fontWeight: FontWeight.w900),
  //         child: Text(secondToTimeStamp(snapshot?.data ?? widget.time)),
  //       );
  //     },
  //   );
  // }

  Color getColor(int value) {
    if (value < 300) return Colors.red.shade900;
    if (value < (widget.time / 2))
      return Colors.red.shade400;
    else
      return Colors.black;
  }

  String secondToTimeStamp(int value) {
    int hr = (value / 3600).floor();
    int min = ((value % 3600) / 60).floor();
    int second = (value % 3600) % 60.floor();
    // if (widget.isExamTimer)
    //   return (hr < 10 ? '0' : '') +
    //       hr.toString() +
    //       ':' +
    //       (min < 10 ? '0' : '') +
    //       min.toString();
    // else
    return (hr < 10 ? '0' : '') +
        hr.toString() +
        ':' +
        (min < 10 ? '0' : '') +
        min.toString() +
        ':' +
        (second < 10 ? '0' : '') +
        second.toString();
  }

  buildLoadingWidget() {
    return Center(child: CircularProgressIndicator());
  }

  buildEmptyWidget() {
    return emptyDataWidget();
  }

  Future<List<Question>> fetchQuestions() async {
    return await QuestionService().getExamQuestions(widget.chapterId);
  }
}
