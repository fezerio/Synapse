import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:photo_view/photo_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/discussionmodel.dart';
import 'package:synapse/page/askAnything/postdetailpage.dart';
import 'package:synapse/service/discussionservice.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';
import '../../model/discussionpost.dart';
import '../../config/constant.dart';

class PostPage extends StatefulWidget {
  final List<DiscussionModel> modelData;
  final Function(List<DiscussionModel>) onDataUpdate;

  const PostPage({Key key, this.modelData, this.onDataUpdate})
      : super(key: key);
  @override
  _PostPageState createState() => _PostPageState();
}

///postStatus=0->not posting, 1-> posting on progress 2-> post completed 3->error
class _PostPageState extends State<PostPage> {
  RefreshController refreshController = RefreshController();
  List<DiscussionModel> data;
  ValueNotifier<int> postStatus = ValueNotifier(0);
  final double avatarSize = 60;
  final double postSize = 90;
  List<File> realImage = [];
  ValueNotifier<bool> allowPost = ValueNotifier(false);
  fetchData(bool isRefresh) async {
    List<DiscussionModel> temp;
    if (!isRefresh) {
      if (widget.modelData == null) {
        temp = await DiscussionService().getDiscussionPost();
        // refreshController.refreshCompleted();
        widget.onDataUpdate(temp);
        if (this.mounted)
          setState(() {
            data = temp;
          });
      } else
        // setState(() {
        data = widget.modelData;
      // });
    } else {
      temp = await DiscussionService().getDiscussionPost();
      widget.onDataUpdate(temp);
      refreshController.refreshCompleted();
      if (this.mounted)
        setState(() {
          data = temp;
        });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchData(false);
  }

  postDiscussion(
      String discussion, List<String> images, List<String> fileName) async {
    postStatus.value = 1;
    final id = await DiscussionService()
        .insertDiscussionPost(discussion, images, fileName);
    int idValue = int.tryParse(id.toString()) ?? -1;
    if (idValue == -1) {
      postStatus.value = 3;
      Toast.show('Error Posting Discussion', context, duration: 5);
      Future.delayed(Duration(seconds: 3)).then((value) {
        postStatus.value = 0;
      });
      return;
    } else {
      postStatus.value = 2;
      DiscussionModel post = DiscussionModel(
          attachments: [...realImage.map((e) => e.path)],
          discussionPost: DiscussionPost(
              post: discussion,
              islocal: true,
              id: idValue,
              created_user_id: PreferenceService().userUid,
              created_user_image: PreferenceService().photoUrl,
              created_user_name: PreferenceService().displayName,
              created_date: DateTime.now().toUtc().toIso8601String(),
              user_id: PreferenceService().userUid));
      addPost(post);
      await deleteCompressedImage();
      Future.delayed(Duration(milliseconds: 1400)).then((value) {
        postStatus.value = 0;
      });
    }
  }

  addPost(DiscussionModel post) {
    List<DiscussionModel> temp = [];
    temp.add(post);
    temp.addAll(data);
    attachments.clear();
    allowPost.value = false;
    postController.clear();
    if (this.mounted)
      setState(() {
        data = temp;
      });
  }

  List<File> attachments = [];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: appColor,
      child: Column(
        children: <Widget>[
          ValueListenableBuilder(
            valueListenable: postStatus,
            builder: (context, value, child) => ((value ?? 0) != 0)
                ? LinearProgressIndicator(
                    value: value == 1 ? null : 100,
                    valueColor: AlwaysStoppedAnimation(value == 1
                        ? Colors.blue[700]
                        : value == 3 ? Colors.red : Colors.green),
                  )
                : Container(),
          ),
          Expanded(
            child: (data == null)
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SmartRefresher(
                    physics: BouncingScrollPhysics(),
                    controller: refreshController,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(12, 15, 12, 5),
                      child: getPostList(),
                    ),
                    onRefresh: () {
                      fetchData(true);
                    },
                  ),
          ),
        ],
      ),
    );
  }

  Widget getPostList() {
    if (data.isEmpty) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          userPostSubmissionWidget(),
          // Expanded(
          //   child: Center(
          //     child: Image.asset(
          //       'assets/images/empty-box.png',
          //       height: 100,
          //       width: 100,
          //       fit: BoxFit.contain,
          //     ),
          //   ),
          // ),
        ],
      );
    }
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: Column(
        children: [
          userPostSubmissionWidget(),
          SizedBox(
            height: 12,
          ),
          ListView.builder(
            itemCount: data.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) => postWidget(data[index]),
          )
          // ...data.map((e) => postWidget(e)).toList()
        ],
      ),
    );
  }

  Widget postWidget(DiscussionModel postData) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Card(
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(CupertinoPageRoute(
              builder: (context) => PostDetailPage(
                post: postData.discussionPost,
                attachments: postData.attachments,
              ),
            ));
          },
          child: cardWidget(postData),
        ),
      ),
    );
  }

  Widget cardWidget(DiscussionModel postData) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: (postData.discussionPost.created_user_image ?? '')
                        .isEmpty
                    ? Image.asset(
                        userPicturePlaceholder,
                        height: avatarSize,
                        width: avatarSize,
                        fit: BoxFit.fill,
                      )
                    : (postData.discussionPost.created_user_id ==
                            PreferenceService().userUid)
                        ? Image.file(
                            File(PreferenceService().photoUrl),
                            height: avatarSize,
                            width: avatarSize,
                            fit: BoxFit.fill,
                            errorBuilder: (context, url, error) => Image.asset(
                              userPicturePlaceholder,
                              height: avatarSize,
                              width: avatarSize,
                              fit: BoxFit.fill,
                            ),
                          )
                        : Image.network(
                            baseUrl +
                                postData.discussionPost.created_user_image,
                            height: avatarSize,
                            width: avatarSize,
                            fit: BoxFit.fill,
                            errorBuilder: (context, url, error) => Image.asset(
                              userPicturePlaceholder,
                              height: avatarSize,
                              width: avatarSize,
                              fit: BoxFit.fill,
                            ),
                          ),
              ),
              SizedBox(
                width: 8,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    postData.discussionPost.created_user_name ?? 'Unknown',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    timeago.format(DateTime.tryParse(
                            postData.discussionPost.created_date) ??
                        DateTime.now()),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                        fontSize: 15),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            thickness: 2.5,
          ),
          Text(postData.discussionPost.post),
          if (postData.attachments != null)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: postData.attachments
                      .map(
                        (e) => Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: InkWell(
                            onTap: () {
                              showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (context) => PhotoView(
                                    backgroundDecoration: BoxDecoration(
                                        color: Colors.transparent),
                                    imageProvider: CachedNetworkImageProvider(
                                      (postData.discussionPost.islocal ?? false)
                                          ? e
                                          : baseUrl + e,
                                    )),
                              );
                            },
                            child: CachedNetworkImage(
                              imageUrl:
                                  (postData.discussionPost.islocal ?? false)
                                      ? e
                                      : baseUrl + e,
                              height: postSize,
                              width: postSize,
                              fit: BoxFit.fill,
                              errorWidget: (context, url, error) => Image.asset(
                                imagePlaceholder,
                                height: postSize,
                                width: postSize,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget userPostSubmissionWidget() {
    return Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: File(PreferenceService().photoUrl).existsSync()
                          ? Image.file(
                              File(PreferenceService().photoUrl),
                              height: avatarSize,
                              width: avatarSize,
                              fit: BoxFit.fill,
                              errorBuilder: (context, error, stackTrace) =>
                                  Image.asset(
                                userPicturePlaceholder,
                                height: avatarSize,
                                width: avatarSize,
                                fit: BoxFit.fill,
                              ),
                            )
                          : Image.asset(
                              userPicturePlaceholder,
                              height: avatarSize,
                              width: avatarSize,
                              fit: BoxFit.fill,
                            ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: postController,
                        onChanged: (value) {
                          if (value.isNotEmpty) {
                            if (!allowPost.value) {
                              allowPost.value = true;
                            }
                          } else {
                            if (allowPost.value) {
                              allowPost.value = false;
                            }
                          }
                        },
                        maxLines: 4,
                        decoration: InputDecoration.collapsed(
                            hintText: 'What do you want to ask?'),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                          children: attachments
                              .map((e) => Padding(
                                    padding: const EdgeInsets.only(right: 10),
                                    child: InkWell(
                                      onTap: () {
                                        showPhotoView(e.path, context, true);
                                      },
                                      child: Stack(
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 6, right: 6),
                                                child: Image.file(
                                                  e,
                                                  height: avatarSize + 10,
                                                  width: avatarSize + 10,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Positioned(
                                            right: 0,
                                            child: InkWell(
                                              onTap: () {
                                                setState(() {
                                                  attachments.remove(e);
                                                });
                                              },
                                              child: Icon(
                                                MdiIcons.closeCircle,
                                                color: Colors.red,
                                                size: 24,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ))
                              .toList()),
                    )),
                    InkWell(
                      onTap: () async {
                        PermissionStatus status =
                            await Permission.storage.request();
                        if (!status.isGranted) {
                          Toast.show('Permission Required', context);
                          return;
                        }
                        if (Platform.isIOS)
                          pickImageUsingImagePicker();
                        else
                          pickImageUsingFilePicker();
                      },
                      child: CircleAvatar(
                        radius: avatarSize / 4,
                        child: Icon(Icons.add),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Material(
            elevation: 1,
            child: ValueListenableBuilder(
                valueListenable: allowPost,
                builder: (context, value, snapshot) {
                  bool temp = attachments.isNotEmpty || value;
                  return InkWell(
                    onTap: () async {
                      List<String> images = [];
                      List<String> filename = [];
                      if (attachments != null) {
                        for (int i = 0; i < attachments.length; i++) {
                          final element = attachments[i];
                          final temp = await File(element.path).readAsBytes();
                          images.add(base64Encode(temp));
                          filename.add((PreferenceService().userUid +
                                      DateTime.now().toUtc().toIso8601String())
                                  .toString()
                                  .replaceAll('-', '')
                                  .replaceAll(':', '')
                                  .replaceAll('.', '') +
                              "." +
                              element.path.split('/').last.split('.').last);
                        }
                      }

                      postDiscussion(postController.text, images, filename);
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 400),
                      height: !temp ? 0 : 40,
                      color: appColor,
                      child: !temp
                          ? SizedBox.shrink()
                          : Center(
                              child: Text(
                                'POST',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            ),
                      width: double.maxFinite,
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  pickImageUsingFilePicker() async {
    FilePickerResult files;
    try {
      files = await FilePicker.platform
          .pickFiles(allowMultiple: true, type: FileType.image);
    } catch (e) {
      Toast.show(e.message ?? e, context);
    }
    if (files != null) {
      for (int i = 0; i < files.files.length; i++) {
        PlatformFile file = files.files[i];
        File temp = await getCompressImage(File(file.path));
        realImage.add(File(file.path));
        attachments.add(temp);
        compressedImage.add(temp);
      }
      setState(() {});
    }
  }

  pickImageUsingImagePicker() async {
    PickedFile files;
    try {
      files = await ImagePicker().getImage(source: ImageSource.gallery);
    } catch (e) {
      Toast.show(e.message ?? e, context);
    }
    if (files != null) {
      File temp = await getCompressImage(File(files.path));
      realImage.add(File(files.path));
      attachments.add(temp);
      compressedImage.add(temp);
      setState(() {});
    }
  }

  Future<File> getCompressImage(File file) async {
    String ext = file.path.split('/').last.split('.').last;
    String targetPath = (await getApplicationSupportDirectory()).path +
        DateTime.now().toIso8601String() +
        '.' +
        ext;
    if (ext != 'jpeg' || ext != 'jpg' || ext != 'png') {
      return file;
    }
    try {
      var result = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path,
        targetPath,
        format: ext == 'png' ? CompressFormat.png : CompressFormat.jpeg,
        quality: 88,
      );
      return result;
    } catch (e) {
      return file;
    }
  }

  List<File> compressedImage = [];

  deleteCompressedImage() async {
    for (int i = 0; i < compressedImage.length; i++) {
      File temp = compressedImage[i];
      try {
        await temp.delete();
      } catch (e) {}
    }
  }

  TextEditingController postController = TextEditingController();
}
