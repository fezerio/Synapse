import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/discussionmodel.dart';
import 'package:synapse/model/discussionpost.dart';
import 'package:synapse/service/discussionservice.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:photo_view/photo_view.dart';

class PostDetailPage extends StatefulWidget {
  final DiscussionPost post;
  final List<String> attachments;

  const PostDetailPage({Key key, this.post, this.attachments})
      : super(key: key);
  @override
  _PostDetailPageState createState() => _PostDetailPageState();
}

class _PostDetailPageState extends State<PostDetailPage> {
  ValueNotifier<List<DiscussionModel>> comments = ValueNotifier([]);

  fetchData() async {
    comments.value =
        await DiscussionService().getDiscussionComment(widget.post.id);
  }

  final double avatarSize = 50;
  final double postSize = 90;
  // TextEditingController controller = TextEditingController();
  ValueNotifier<bool> allowPost = ValueNotifier(false);
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  TextEditingController postController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.9),
      appBar: SynapseAppBar(
        title: widget.post.created_user_name ?? 'Unknown',
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(12, 15, 12, 5),
          child: Column(
            children: <Widget>[
              Material(
                color: Colors.white,
                elevation: 1,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
                    child: cardWidget(DiscussionModel(
                        attachments: widget.attachments,
                        discussionPost: widget.post)),
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  ValueListenableBuilder<List<DiscussionModel>>(
                    valueListenable: comments,
                    builder: (context, value, child) {
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: value.length,
                        itemBuilder: (context, index) => Column(
                          children: <Widget>[
                            Material(
                              color: Colors.white,
                              elevation: 1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15, 10, 10, 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(6),
                                      child: (value[index]
                                                      .discussionPost
                                                      .created_user_image ??
                                                  '')
                                              .isEmpty
                                          ? Image.asset(
                                              userPicturePlaceholder,
                                              height: avatarSize,
                                              width: avatarSize,
                                              fit: BoxFit.fill,
                                            )
                                          : Image.network(
                                              baseUrl +
                                                  value[index]
                                                      .discussionPost
                                                      .created_user_image,
                                              height: avatarSize,
                                              width: avatarSize,
                                              fit: BoxFit.fill,
                                              errorBuilder:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                userPicturePlaceholder,
                                                height: avatarSize,
                                                width: avatarSize,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              value[index]
                                                      .discussionPost
                                                      .created_user_name ??
                                                  'Unknown',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                            SizedBox(
                                              width: 3,
                                            ),
                                            Icon(
                                              MdiIcons.circleMedium,
                                              size: 18,
                                              color: Colors.blue[800],
                                            ),
                                            Text(
                                              timeago.format(DateTime.tryParse(
                                                      value[index]
                                                          .discussionPost
                                                          .created_date) ??
                                                  DateTime.now()),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey,
                                                  fontSize: 15),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(value[index].discussionPost.post),
                                        if (value[index].attachments != null)
                                          if (value[index]
                                              .attachments
                                              .isNotEmpty)
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: SingleChildScrollView(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                child: Row(
                                                  children: value[index]
                                                      .attachments
                                                      .map(
                                                        (e) => Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 15),
                                                          child: InkWell(
                                                            onTap: () {
                                                              showDialog(
                                                                context:
                                                                    context,
                                                                barrierDismissible:
                                                                    true,
                                                                builder: (context) =>
                                                                    PhotoView(
                                                                        backgroundDecoration: BoxDecoration(
                                                                            color: Colors
                                                                                .transparent),
                                                                        imageProvider:
                                                                            CachedNetworkImageProvider(
                                                                          baseUrl +
                                                                              e,
                                                                        )),
                                                              );
                                                            },
                                                            child:
                                                                CachedNetworkImage(
                                                              imageUrl:
                                                                  baseUrl + e,
                                                              height:
                                                                  avatarSize,
                                                              width: avatarSize,
                                                              fit: BoxFit.fill,
                                                              errorWidget: (context,
                                                                      url,
                                                                      error) =>
                                                                  Image.asset(
                                                                userPicturePlaceholder,
                                                                height:
                                                                    avatarSize,
                                                                width:
                                                                    avatarSize,
                                                                fit:
                                                                    BoxFit.fill,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                      .toList(),
                                                ),
                                              ),
                                            ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  Material(
                    elevation: 1,
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 10, 10),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: File(PreferenceService().photoUrl)
                                        .existsSync()
                                    ? Image.file(
                                        File(PreferenceService().photoUrl),
                                        height: avatarSize,
                                        width: avatarSize,
                                        fit: BoxFit.fill,
                                        errorBuilder:
                                            (context, error, stackTrace) =>
                                                Image.asset(
                                          userPicturePlaceholder,
                                          height: avatarSize,
                                          width: avatarSize,
                                          fit: BoxFit.fill,
                                        ),
                                      )
                                    : Image.asset(
                                        userPicturePlaceholder,
                                        height: avatarSize,
                                        width: avatarSize,
                                        fit: BoxFit.fill,
                                      ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: postController,
                                  maxLines: 1,
                                  onChanged: (value) {
                                    if (value.isNotEmpty) {
                                      if (!allowPost.value) {
                                        allowPost.value = true;
                                      }
                                    } else {
                                      if (allowPost.value) {
                                        allowPost.value = false;
                                      }
                                    }
                                  },
                                  decoration: InputDecoration.collapsed(
                                      hintText: 'Enter your reply here..'),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                    children: attachments
                                        .map((e) => Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 10),
                                              child: InkWell(
                                                onTap: () {
                                                  showPhotoView(
                                                      e.path, context, true);
                                                },
                                                child: Stack(
                                                  children: <Widget>[
                                                    Column(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 6,
                                                                  right: 6),
                                                          child: Image.file(
                                                            e,
                                                            height:
                                                                avatarSize + 10,
                                                            width:
                                                                avatarSize + 10,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Positioned(
                                                      right: 0,
                                                      child: InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            attachments
                                                                .remove(e);
                                                          });
                                                        },
                                                        child: Icon(
                                                          MdiIcons.closeCircle,
                                                          color: Colors.red,
                                                          size: 24,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ))
                                        .toList()),
                              )),
                              InkWell(
                                onTap: () async {
                                  PermissionStatus status =
                                      await Permission.storage.request();
                                  if (!status.isGranted) {
                                    Toast.show('Permission Required', context);
                                    return;
                                  }
                                  if (Platform.isIOS)
                                    pickImageUsingImagePicker();
                                  else
                                    pickImageUsingFilePicker();
                                },
                                child: CircleAvatar(
                                  radius: avatarSize / 4,
                                  child: Icon(Icons.add),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Material(
                    elevation: 1,
                    child: ValueListenableBuilder(
                        valueListenable: allowPost,
                        builder: (context, value, snapshot) {
                          bool temp = attachments.isNotEmpty || value;
                          return InkWell(
                            onTap: () async {
                              postComment(postController.text);
                              List<String> images = [];
                              List<String> filename = [];
                              if (attachments != null) {
                                //   for (int i = 0; i < attachments.length; i++) {
                                //     final element = attachments[i];
                                //     final temp =
                                //         await File(element.path).readAsBytes();
                                //     images.add(base64Encode(temp));
                                //     filename.add((PreferenceService().userUid +
                                //                 DateTime.now()
                                //                     .toUtc()
                                //                     .toIso8601String())
                                //             .toString()
                                //             .replaceAll('-', '')
                                //             .replaceAll(':', '')
                                //             .replaceAll('.', '') +
                                //         "." +
                                //         element.path
                                //             .split('/')
                                //             .last
                                //             .split('.')
                                //             .last);
                                //   }
                              }

                              // postDiscussion(
                              //     postController.text, images, filename);
                            },
                            child: AnimatedContainer(
                              duration: Duration(milliseconds: 400),
                              height: !temp ? 0 : 40,
                              color: appColor,
                              child: !temp
                                  ? SizedBox.shrink()
                                  : Center(
                                      child: Text(
                                        'COMMENT',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),
                              width: double.maxFinite,
                            ),
                          );
                        }),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  pickImageUsingFilePicker() async {
    FilePickerResult files;
    try {
      files = await FilePicker.platform
          .pickFiles(allowMultiple: true, type: FileType.image);
    } catch (e) {
      Toast.show(e.message ?? e, context);
    }
    if (files != null) {
      for (int i = 0; i < files.files.length; i++) {
        PlatformFile file = files.files[i];
        File temp = await getCompressImage(File(file.path));
        attachments.add(temp);
        compressedImage.add(temp);
      }
      setState(() {});
    }
  }

  pickImageUsingImagePicker() async {
    PickedFile files;
    try {
      files = await ImagePicker().getImage(source: ImageSource.gallery);
    } catch (e) {
      Toast.show(e.message ?? e, context);
    }
    if (files != null) {
      File temp = await getCompressImage(File(files.path));
      attachments.add(temp);
      compressedImage.add(temp);
      setState(() {});
    }
  }

  List<File> attachments = [];

  postComment(String value) async {
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: "Posting Comment");
    await pr.show();
    List<File> files = attachments;
    List<String> images = [];
    List<String> filename = [];
    for (int i = 0; i < files.length; i++) {
      final element = files[i];
      final temp = await File(element.path).readAsBytes();
      images.add(base64Encode(temp));
      filename.add((PreferenceService().userUid +
                  DateTime.now().toUtc().toIso8601String())
              .toString()
              .replaceAll('-', '')
              .replaceAll(':', '')
              .replaceAll('.', '') +
          "." +
          element.path.split('/').last.split('.').last);
    }
    bool callback = await DiscussionService()
        .insertDiscussionComment(value, widget.post.id, images, filename);
    await deleteCompressedImage();
    postController.clear();
    attachments.clear();
    allowPost.value = false;
    await pr.hide();
    if (callback) {
      Toast.show('Comment Posted', context);
      fetchData();
    } else {
      Toast.show('Error Posting Comment', context);
    }
  }

  Widget cardWidget(DiscussionModel postData) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: (postData.discussionPost.created_user_image ?? '').isEmpty
                  ? Image.asset(
                      userPicturePlaceholder,
                      height: avatarSize,
                      width: avatarSize,
                      fit: BoxFit.fill,
                    )
                  : Image.network(
                      baseUrl + postData.discussionPost.created_user_image,
                      height: avatarSize,
                      width: avatarSize,
                      fit: BoxFit.fill,
                      errorBuilder: (context, url, error) => Image.asset(
                        userPicturePlaceholder,
                        height: avatarSize,
                        width: avatarSize,
                        fit: BoxFit.fill,
                      ),
                    ),
            ),
            SizedBox(
              width: 8,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  postData.discussionPost.created_user_name ?? 'Unknown',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  timeago.format(
                      DateTime.tryParse(postData.discussionPost.created_date) ??
                          DateTime.now()),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey,
                      fontSize: 15),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Divider(
          thickness: 2.5,
        ),
        Text(postData.discussionPost.post),
        if (postData.attachments != null)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: postData.attachments
                    .map(
                      (e) => Padding(
                        padding: const EdgeInsets.only(right: 15),
                        child: InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (context) => PhotoView(
                                  backgroundDecoration:
                                      BoxDecoration(color: Colors.transparent),
                                  imageProvider: CachedNetworkImageProvider(
                                    baseUrl + e,
                                  )),
                            );
                          },
                          child: CachedNetworkImage(
                            imageUrl: baseUrl + e,
                            height: postSize,
                            width: postSize,
                            fit: BoxFit.fill,
                            errorWidget: (context, url, error) => Image.asset(
                              imagePlaceholder,
                              height: postSize,
                              width: postSize,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ),
      ],
    );
  }

  Future<File> getCompressImage(File file) async {
    String ext = file.path.split('/').last.split('.').last;
    String targetPath = (await getApplicationSupportDirectory()).path +
        DateTime.now().toIso8601String() +
        '.' +
        ext;
    if (ext != 'jpeg' || ext != 'jpg' || ext != 'png') {
      return file;
    }
    try {
      var result = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path,
        targetPath,
        format: ext == 'png' ? CompressFormat.png : CompressFormat.jpeg,
        quality: 88,
      );
      return result;
    } catch (e) {
      return file;
    }
  }

  List<File> compressedImage = [];

  deleteCompressedImage() async {
    for (int i = 0; i < compressedImage.length; i++) {
      File temp = compressedImage[i];
      try {
        await temp.delete();
      } catch (e) {}
    }
  }
}
