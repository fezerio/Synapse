import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/subject.dart';
import 'package:synapse/model/topic.dart';
import 'package:synapse/page/subject/subjectHome.dart';
import 'package:synapse/page/subject/subjectslist.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/widget/synapseappbar.dart';

class TopicsList extends StatefulWidget {
  final List<Topic> topic;
  const TopicsList({Key key, this.topic}) : super(key: key);
  @override
  _TopicsListState createState() => _TopicsListState();
}

class _TopicsListState extends State<TopicsList> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Subjects',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10, top: 10, right: 0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: widget.topic.map((e) => topicWidget(e)).toList()),
          )
        ],
      ),
    );
  }

  Widget topicWidget(Topic topic) {
    return InkWell(
      splashColor: Colors.transparent,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        Navigator.of(context).push(CupertinoPageRoute(
          builder: (context) => SubjectListPage(
            topicId: topic.id,
            name: topic.name,
          ),
        ));
      },
      child: Container(
        height: 45,
        padding: EdgeInsets.symmetric(
            horizontal: ScreenSizeConfig.safeBlockHorizontal * 5, vertical: 0),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 5,
                offset: Offset(0, 4),
                spreadRadius: 2)
          ],
          borderRadius: BorderRadius.circular(15),
        ),
        child: Center(
          child: Text(
            topic?.name?.replaceFirst(' ', '\n') ?? '',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500, letterSpacing: 1.5, fontSize: 15),
          ),
        ),
      ),
    );
  }
}
