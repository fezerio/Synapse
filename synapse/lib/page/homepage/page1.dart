import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/SummaryInfo.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/errorHandler.dart';
import 'package:synapse/model/mcqQuestion.dart';
import 'package:synapse/model/resultAnswer.dart';
import 'package:synapse/page/exam/examHomePage.dart';
import 'package:synapse/page/exam/resultPage.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:synapse/widget/liveanimation.dart';
import 'package:toast/toast.dart';

import 'package:synapse/model/question.dart';
import 'package:synapse/widget/countdownTimer.dart';
import 'package:synapse/widget/interactiveQuestion.dart';

class MCQOfTheDay extends StatefulWidget {
  final List<MCQQuestion> questions;
  final Function(bool) isCorrect;
  const MCQOfTheDay({
    Key key,
    this.questions,
    this.isCorrect,
  }) : super(key: key);
  @override
  _MCQOfTheDayState createState() => _MCQOfTheDayState();
}

class _MCQOfTheDayState extends State<MCQOfTheDay> {
  DateTime _currenTime;
  List<MCQQuestion> snapshot;
  int temp = 1;
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  @override
  void initState() {
    _currenTime = DateTime.now().toUtc();
    super.initState();
  }

  void refreshQuestion() async {
    snapshot = await QuestionService().getMCQQuestion();
    refreshController.refreshCompleted();
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.questions != null && temp == 1) {
      snapshot = widget.questions;
      temp = 2;
    }
    if (snapshot != null)
      return (snapshot.isEmpty)
          ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Text(
                      'No Questions Today, Next Question On',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  TimerWidget(
                    duration: (DateTime.utc(_currenTime.year, _currenTime.month,
                                (_currenTime.day + 1), 0, 0, 0, 0, 0)
                            .difference(_currenTime))
                        .inSeconds,
                    onCountDownFinish: () {
                      snapshot = null;
                      _currenTime = DateTime.now().toUtc();
                      setState(() {});
                      refreshQuestion();
                    },
                  )
                ],
              ),
            )
          : Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text('MCQ of the Day',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: Colors.blue[900],
                          )),
                      if (snapshot[0].isSolved)
                        Row(
                          children: <Widget>[
                            Text(' ( ',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.blue[900],
                                )),
                            TimerWidget(
                              isExamTimer: true,
                              isMCQ: true,
                              duration: (DateTime.utc(
                                          _currenTime.year,
                                          _currenTime.month,
                                          (_currenTime.day + 1),
                                          0,
                                          0,
                                          0,
                                          0,
                                          0)
                                      .difference(_currenTime))
                                  .inSeconds,
                              onCountDownFinish: () {
                                snapshot = null;
                                _currenTime = DateTime.now().toUtc();
                                setState(() {});
                                refreshQuestion();
                              },
                            ),
                            Text(' )',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.blue[900],
                                )),
                          ],
                        )
                    ],
                  ),
                ),
                // SizedBox(
                //   height: 5,
                // ),
                IgnorePointer(
                  ignoring: snapshot[0].isSolved,
                  child: InteractiveQuestion(
                    revealAnswer: snapshot[0].isSolved,
                    isLastPage: true,
                    showButtons: !snapshot[0].isSolved,
                    isTest: false,
                    revealAnswerData: {
                      'userAnswer': snapshot[0].userAnswer,
                      'answer': snapshot[0].question.answer
                    },
                    question: snapshot[0].question,
                    onSubmit: (isTrue, userAnswer) async {
                      // print(userAnswer);
                      // return;
                      ProgressDialog dialog = ProgressDialog(context,
                          isDismissible: false,
                          type: ProgressDialogType.Normal);
                      dialog.style(message: "Submitting Answer");
                      await dialog?.show();
                      try {
                        await QuestionService().questionSolved(
                            questionId: snapshot[0].question.id,
                            chapterId: snapshot[0].question.chapter_id,
                            correctAnswer: snapshot[0].question.answer,
                            userAnswer: userAnswer);
                        await dialog?.hide();
                        snapshot = null;
                        widget.isCorrect(isTrue);
                        setState(() {});
                        refreshQuestion();
                      } on ErrorHandler catch (e) {
                        await dialog.hide();
                        Toast.show(e.message, context, duration: 5);
                      }
                    },
                    onNextPress: (isTrue, userAnswer) {
                      Toast.show(isTrue.toString(), context);
                    },
                  ),
                ),
              ],
            );
    else
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 55),
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
  }
}

class MockTestData extends StatefulWidget {
  final List<Chapter> mocktestData;

  const MockTestData({Key key, this.mocktestData}) : super(key: key);

  @override
  _MockTestDataState createState() => _MockTestDataState();
}

class _MockTestDataState extends State<MockTestData> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
          child: Text('Mock Test',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
        ),
        SizedBox(
          height: 10,
        ),
        if (widget.mocktestData == null)
          Center(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: CircularProgressIndicator()),
          )
        else if ((widget.mocktestData).length > 0)
          SizedBox(
            height: ScreenSizeConfig.blockSizeHorizontal * 40,
            width: double.maxFinite,
            child: PageView.builder(
              controller: PageController(viewportFraction: 0.8),
              scrollDirection: Axis.horizontal,
              itemCount: widget.mocktestData.length,
              itemBuilder: (context, index) => Padding(
                  padding:
                      const EdgeInsets.only(bottom: 10, top: 10, right: 25),
                  child: builder(widget.mocktestData[index], context)),
            ),
          )
        else
          Center(
            child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  'No Test Scheduled Yet',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                )),
          ),
      ],
    );
  }

  Widget builder(Chapter e, BuildContext context) {
    final daysLeft = DateTime.parse(e.live_date)
        .difference(DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().toUtc().day))
        .inDays;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 5,
                offset: Offset(0, 4),
                spreadRadius: 2)
          ],
          borderRadius: BorderRadius.circular(15),
        ),
        child: Material(
          color: (daysLeft <= 0) ? Colors.white : null,
          child: InkWell(
            // splashColor: Colors.transparent,
            // focusColor: Colors.transparent,
            // hoverColor: Colors.transparent,
            // highlightColor: Colors.transparent,
            onTap: (daysLeft > 0)
                ? null
                : () {
                    Navigator.of(context)
                        .push(CupertinoPageRoute(
                      builder: (context) => (e.completed)
                          ? ResultPage(
                              chapterId: e.id,
                              chapterName: e.name,
                              timer: e.timer,
                              isMockTest: true,
                            )
                          : ExamPage(
                              chapterId: e.id,
                              chapterName: e.name,
                              title: '',
                              isMockTest: true,
                              time: e.timer,
                            ),
                    ))
                        .then((value) {
                      setState(() {});
                    });
                  },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    e.name,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        letterSpacing: 1.5,
                        fontSize: 16),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  daysLeft <= 0
                      ? LiveAnimationFlip()
                      : Text(
                          daysLeft.toString() +
                              (daysLeft == 1 ? ' Day ' : ' Days ') +
                              'Left',
                          textAlign: TextAlign.center,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.w800,
                              color: appColor,
                              letterSpacing: 1.5,
                              fontSize: 16),
                        ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        e.no_of_questions.toString() + " Questions",
                        textAlign: TextAlign.center,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            letterSpacing: 1.5,
                            color: Colors.grey,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.grey,
                        radius: 3,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        (e.timer / 3600).toStringAsFixed(0) + ' hr',
                        textAlign: TextAlign.center,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            letterSpacing: 1.5,
                            color: Colors.grey,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SuggestedTest extends StatelessWidget {
  final List<Chapter> suggestedTest;

  const SuggestedTest({Key key, this.suggestedTest}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if ((suggestedTest ?? []).length > 0)
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
            child: Text('Suggested Tests',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
          ),
          SizedBox(
            height: 15,
          ),
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10, top: 10, right: 0),
              child: Row(
                  children: suggestedTest
                      .map(
                        (e) => builder(e, context),
                      )
                      .toList()),
            ),
          )
        ],
      );
    return Container(
      height: 1,
      width: 1,
    );
  }

  Widget builder(Chapter data, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: InkWell(
        splashColor: Colors.transparent,
        focusColor: Colors.transparent,
        hoverColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          Navigator.of(context).push(CupertinoPageRoute(
            builder: (context) => (data.completed)
                ? ResultPage(
                    chapterId: data.id,
                    chapterName: data.name,
                    timer: data.timer,
                    isMockTest: false,
                  )
                : ExamPage(
                    chapterId: data.id,
                    chapterName: data.name,
                    title: '',
                    isMockTest: false,
                    time: data.timer,
                  ),
          ));
        },
        child: Container(
          height: 90,
          width: ScreenSizeConfig.safeBlockHorizontal * 40,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 5,
                  offset: Offset(0, 4),
                  spreadRadius: 2)
            ],
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Center(
              child: Text(
                data.name,
                textAlign: TextAlign.center,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                    fontSize: 15),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
