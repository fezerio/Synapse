import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/SummaryInfo.dart';
import 'package:synapse/model/bookmarkData.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/mcqQuestion.dart';
import 'package:synapse/model/question.dart';
import 'package:synapse/model/topic.dart';
import 'package:synapse/page/homepage/page1.dart';
import 'package:synapse/page/homepage/page2.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/preferenceservice.dart';

class HomePage extends StatefulWidget {
  final List<MCQQuestion> questions;
  final List<Chapter> suggestedTest;
  final List<Chapter> mockTest;
  final SummaryInfo summaryInfo;
  HomePage(
      {Key key,
      this.questions,
      this.suggestedTest,
      this.summaryInfo,
      this.mockTest})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ValueNotifier<SummaryInfo> summaryInfo = ValueNotifier(
      SummaryInfo(mcqRightAnswer: 0, mcqWrongAnswer: 0, testGiven: 0));

  @override
  void initState() {
    if (widget.summaryInfo != null) {
      if (summaryInfo.value.toString() != widget.summaryInfo.toString()) {
        summaryInfo.value = widget.summaryInfo;
      }
    }
    Timer.periodic(Duration(seconds: 8), (timer) {
      BookmarkService()
          .getMcqTrackingData(PreferenceService().userUid)
          .then((value) {
        if (value != null) {
          if (value.toString() != summaryInfo.value.toString()) {
            summaryInfo.value = value;
          }
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ValueListenableBuilder(
            valueListenable: summaryInfo,
            builder: (context, value, child) =>
                HomeHeaderWidget(sync: summaryInfo.value),
          ),
          SizedBox(
            height: 16,
          ),
          MCQOfTheDay(
            questions: widget.questions,
            isCorrect: (value) {
              summaryInfo.value = SummaryInfo(
                  mcqRightAnswer: value
                      ? summaryInfo.value.mcqRightAnswer + 1
                      : summaryInfo.value.mcqRightAnswer,
                  mcqWrongAnswer: !value
                      ? summaryInfo.value.mcqWrongAnswer + 1
                      : summaryInfo.value.mcqWrongAnswer,
                  testGiven: summaryInfo.value.testGiven);
            },
          ),
          SizedBox(
            height: 15,
          ),
          MockTestData(
            mocktestData: widget.mockTest,
          ),
          SizedBox(
            height: 10,
          ),
          SuggestedTest(suggestedTest: widget.suggestedTest),
          SizedBox(
            height: 10,
          ),
          TopicsList(
            topic: [
              Topic(name: 'Basic Science', id: 1),
              Topic(name: 'Major', id: 2),
              Topic(name: 'Minor', id: 3),
            ],
          )
        ],
      ),
    );
  }
}

class HomeHeaderWidget extends StatefulWidget {
  final SummaryInfo sync;

  const HomeHeaderWidget({Key key, this.sync}) : super(key: key);
  @override
  _HomeHeaderWidgetState createState() => _HomeHeaderWidgetState();
}

class _HomeHeaderWidgetState extends State<HomeHeaderWidget> {
  TextStyle textStyle =
      TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold);
  // int mcqRightAnswer = 0;
  // int mcqWrongAnswer = 0;
  // int totalTestGiven = 0;
  SummaryInfo info;
  // bool oldSync = false;
  fetchMCQinfoData() {
    info = SummaryInfo(
        mcqRightAnswer: widget.sync?.mcqRightAnswer,
        mcqWrongAnswer: widget.sync?.mcqWrongAnswer,
        testGiven: widget.sync?.testGiven);
    // mcqRightAnswer = PreferenceService().mcqRightAnswer;
    // mcqWrongAnswer = PreferenceService().mcqWrongAnswer;
    // totalTestGiven = PreferenceService().totalTestGiven;
  }

  @override
  void initState() {
    super.initState();
    // oldSync = widget.sync;
    fetchMCQinfoData();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.sync.toString() != info.toString()) {
      fetchMCQinfoData();
    }
    final width = ScreenSizeConfig.safeBlockHorizontal * 34;

    return Container(
      color: appColor,
      height: (width / 2) + 10,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 10, bottom: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Attempted Question: ' +
                          ((info.mcqWrongAnswer ?? 0) +
                                  ((info.mcqRightAnswer) ?? 0))
                              .toString(),
                      style: textStyle,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Test Given: ' + (info.testGiven ?? 0).toString(),
                      style: textStyle,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'Right Answer: ' + (info.mcqRightAnswer ?? 0).toString(),
                      style: textStyle,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Wrong Answer: ' + (info.mcqWrongAnswer ?? 0).toString(),
                      style: textStyle,
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
