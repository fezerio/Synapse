import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/widget/chapterWidget.dart';
import '../config/constant.dart';

class MockTestPage extends StatefulWidget {
  final List<Chapter> chapters;
  const MockTestPage({Key key, this.chapters}) : super(key: key);
  @override
  _MockTestPageState createState() => _MockTestPageState();
}

class _MockTestPageState extends State<MockTestPage> {
  // List<Chapter> chatpers;
  int temp = 1;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  Map<String, List<Chapter>> chapterData;

  Map<String, List<Chapter>> groupDataByDate(List<Chapter> chapter) {
    return chapter.groupBy((e) => e.live_date);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (temp == 1 && widget.chapters != null) {
      final chatpers = widget.chapters
          .where((element) => element.no_of_questions > 0)
          .toList();
      chapterData = groupDataByDate(chatpers);

      temp = 2;
    }
    if (chapterData == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else
      return SmartRefresher(
        physics: BouncingScrollPhysics(),
        controller: refreshController,
        onRefresh: () async {
          fetchData();
        },
        child: chapterData.isEmpty
            ? emptyDataWidget()
            : SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 22),
                      child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          final chapters = chapterData.values.toList()[index];
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  DateFormat('dd MMM yyyy').format(
                                    DateTime.parse(chapterData.keys
                                        .toList()[index]
                                        .toString()),
                                  ),
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.7),
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: chapters.length,
                                  itemBuilder: (context, index) => Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 9),
                                    child: ChapterWidget(
                                      onNavigate: () {
                                        fetchData();
                                      },
                                      chapter: chapters[index],
                                      title: chapters[index].name,
                                      isMockTest: true,
                                      isCompleted: chapters[index].completed,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        itemCount: chapterData.length,
                      ),
                    ),
                  ],
                ),
              ),
      );
  }

  fetchData() async {
    final data = await ChapterService().getMockTest();
    chapterData = groupDataByDate(
        data.where((element) => element.no_of_questions > 0).toList());
    refreshController.refreshCompleted();
    if (this.mounted) {
      setState(() {});
    }
  }
}
