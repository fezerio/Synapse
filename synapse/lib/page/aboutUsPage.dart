import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:toast/toast.dart';

import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SynapseAppBar(
        title: ('Contact Us'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // cardWidget(
                  //     context,
                  //     "https://www.w3schools.com/w3css/img_nature.jpg",
                  //     'Fezerio',
                  //     'Kathmandu',
                  //     'https://fezerio.com.com',
                  //     '9860699727'),
                  // SizedBox(height: 20.0),
                  cardWidget(
                      context,
                      "assets/images/subject_placeholder.png",
                      'Sushil Adkhikari',
                      'Nepal',
                      'www.google.com',
                      '12345678'),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Image.asset(
                  'assets/images/fezerio.png',
                  height: 70,
                  width: 120,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _launchURL(url, BuildContext context) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Toast.show('Could not launch', context);
    }
  }

//
  cardWidget(BuildContext context, String networkImagePath, String name,
      String address, String url, String number) {
    return Material(
      color: Colors.white,
      elevation: 5.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: 10.0),
            Image.asset(
              networkImagePath,
              width: 100.0,
              height: 80,
            ),
            SizedBox(width: 10.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Text(
                    address,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Wrap(
                    runSpacing: 5.0,
                    spacing: 5.0,
                    children: [
                      ActionChip(
                        backgroundColor: appColor,
                        avatar: Icon(
                          Icons.call,
                          size: 20.0,
                          color: Colors.white,
                        ),
                        label: Text(
                          'Call',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () =>
                            true ? null : _launchURL('tel:$number', context),
                      ),
                      ActionChip(
                        backgroundColor: appColor,
                        avatar: Icon(
                          Icons.public,
                          size: 20.0,
                          color: Colors.white,
                        ),
                        label: Text(
                          'Visit Website',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () => true ? null : _launchURL(url, context),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
