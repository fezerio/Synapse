import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/leaderboard.dart';
import 'package:synapse/provider/leaderboardProvider.dart';
import 'package:synapse/service/userService.dart';
import 'package:synapse/widget/userCard.dart';

class LeaderBoard extends StatefulWidget {
  // final LeaderBoardProvider leaderBoardData;
  final Function(LeaderBoardProvider) onDataChange;

  const LeaderBoard({Key key, this.onDataChange}) : super(key: key);
  @override
  _LeaderBoardState createState() => _LeaderBoardState();
}

class _LeaderBoardState extends State<LeaderBoard>
    with TickerProviderStateMixin {
  TabController controller;

  ValueNotifier<List<LeaderBoardUserData>> mockTest = ValueNotifier(null);
  ValueNotifier<List<LeaderBoardUserData>> dailyTest = ValueNotifier(null);
  ValueNotifier<List<LeaderBoardUserData>> subjectTest = ValueNotifier(null);

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 3, vsync: this, initialIndex: 0);
    final temp = Provider.of<LeaderBoardProvider>(context, listen: false);
    if (temp != null) {
      mockTest.value = temp.mockTest;
      dailyTest.value = temp.dailyTest;
      subjectTest.value = temp.subjectTest;
    }
  }

  @override
  void dispose() {
    mockTest?.dispose();
    dailyTest?.dispose();
    subjectTest?.dispose();
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
          child: Card(
            elevation: 4,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              labelPadding:
                  const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
              unselectedLabelColor: Colors.black.withOpacity(0.8),
              indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                gradient: LinearGradient(
                  colors: [
                    // appColorDark,
                    appColor,
                    // Color(0xff3a6073),
                    Color(0xff3a7bd5),
                  ],
                  // stops: [1.2, 3],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              ),
              tabs: [
                Text(
                  'Daily Dose',
                  // style: TextStyle(color: Colors.black),
                ),
                Text(
                  'Mock Test',
                  // style: TextStyle(color: Colors.black),
                ),
                Text(
                  'Subject',
                  // style: TextStyle(color: Colors.black),
                ),
              ],
              controller: controller,
            ),
          ),
        ),
        Expanded(
          child: TabBarView(
            children: [
              ValueListenableBuilder<List<LeaderBoardUserData>>(
                valueListenable: dailyTest,
                builder: (context, value, child) => UserListData(
                  data: value,
                  index: 0,
                  onDataChange: (value) {
                    widget.onDataChange(LeaderBoardProvider(
                        dailyTest: value,
                        mockTest: mockTest.value,
                        subjectTest: subjectTest.value));
                  },
                ),
              ),
              ValueListenableBuilder<List<LeaderBoardUserData>>(
                valueListenable: mockTest,
                builder: (context, value, child) => UserListData(
                  data: value,
                  index: 1,
                  onDataChange: (value) {
                    widget.onDataChange(LeaderBoardProvider(
                        dailyTest: dailyTest.value,
                        mockTest: value,
                        subjectTest: subjectTest.value));
                  },
                ),
              ),
              ValueListenableBuilder<List<LeaderBoardUserData>>(
                valueListenable: subjectTest,
                builder: (context, value, child) => UserListData(
                  index: 2,
                  data: value,
                  onDataChange: (value) {
                    widget.onDataChange(LeaderBoardProvider(
                        dailyTest: dailyTest.value,
                        mockTest: mockTest.value,
                        subjectTest: value));
                  },
                ),
              ),
            ],
            controller: controller,
          ),
        )
      ],
    );
  }
}

class UserListData extends StatefulWidget {
  final int index;
  final List<LeaderBoardUserData> data;

  final Function(List<LeaderBoardUserData>) onDataChange;

  const UserListData({Key key, this.data, this.index, this.onDataChange})
      : super(key: key);

  @override
  _UserListDataState createState() => _UserListDataState();
}

class _UserListDataState extends State<UserListData> {
  List<LeaderBoardUserData> listData;
  Timer timer;
  @override
  void initState() {
    listData = widget.data;
    int topicId = widget.index == 0 ? 0 : widget.index == 1 ? -1 : 2;
    int subjectId = widget.index == 0 ? 0 : widget.index == 1 ? -1 : 2;
    timer = Timer.periodic(Duration(seconds: 10), (timer) {
      UserService().getLeaderBoardData(topicId, subjectId).then((value) {
        if (!checkIfDataisSame(value)) {
          widget.onDataChange(value);
          setState(() {
            listData = value;
          });
        }
      });
    });
    super.initState();
  }

  bool checkIfDataisSame(List<LeaderBoardUserData> value) {
    bool t = true;
    for (int i = 0; i < value.length; i++) {
      final element = value[i];
      final temp = listData
          .where((element1) => element1.user_id == element.user_id)
          .toList();
      if (temp.isNotEmpty) {
        if (element.right_answer != temp[0].right_answer) {
          t = false;
          break;
        }
      }
    }
    return t;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (listData == null) {
      return Center(child: CircularProgressIndicator());
    }
    if (listData.isEmpty) {
      return emptyDataWidget();
    }
    return ListView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: listData.length,
        itemBuilder: (context, index) => UserCard(
              rank: index + 1,
              userData: listData[index],
            ));
  }
}
