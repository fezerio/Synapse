import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/errorHandler.dart';
import 'package:synapse/page/mainPage.dart';
import 'package:synapse/page/authentication/passwordResetPage.dart';
import 'package:synapse/service/authservice.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/service/userService.dart';
import 'package:toast/toast.dart';
import '../../model/user.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage>
    with SingleTickerProviderStateMixin {
  TextEditingController _emailAddressController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  bool _obscurePassword = true;
  GlobalKey<FormState> _form = GlobalKey();
  bool isLogin = true;
  AnimationController _controller;
  Animation<double> _initialAnimation, _finalAnimation;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _initialAnimation = Tween(begin: 0.0, end: 1.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });
    _finalAnimation = Tween(begin: 1.0, end: 0.0).animate(_controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Center(
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(
                    vertical: ScreenSizeConfig.safeBlockVertical * 5,
                    horizontal: ScreenSizeConfig.safeBlockHorizontal * 8),
                margin: EdgeInsets.symmetric(
                    vertical: ScreenSizeConfig.safeBlockVertical * 20,
                    horizontal: ScreenSizeConfig.safeBlockHorizontal * 14),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white.withOpacity(0.6),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(
                  ScreenSizeConfig.blockSizeVertical * 5,
                  ScreenSizeConfig.blockSizeVertical * 23,
                  ScreenSizeConfig.blockSizeVertical * 5,
                  ScreenSizeConfig.blockSizeVertical * 4,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        offset: Offset(0, 8),
                        blurRadius: 10)
                  ],
                ),
                child: loginForm(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  clearVariablesData() {
    FocusScope.of(context).requestFocus(FocusNode());
    _passwordController.clear();
    _emailAddressController.clear();
    _nameController.clear();
  }

  Widget loginForm() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(30, 20, 30, 5),
          child: Form(
            key: _form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                SizedBox(height: ScreenSizeConfig.safeBlockVertical * 3),
                Container(
                  height: 50,
                  child: new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: _emailAddressController,
                      validator: validateEmail,
                      style: TextStyle(decoration: TextDecoration.none),
                      decoration:
                          fieldDecoration("Email address", Icon(Icons.email))),
                ),
                SizedBox(height: ScreenSizeConfig.safeBlockVertical * 2),
                Container(
                  height: 50,
                  child: new TextFormField(
                    obscureText: _obscurePassword,
                    keyboardType: TextInputType.visiblePassword,
                    controller: _passwordController,
                    validator: validatePassword,
                    cursorColor: Colors.black,
                    decoration: fieldDecoration(
                        "Password",
                        InkWell(
                          onTap: () {
                            _obscurePassword = !_obscurePassword;
                            setState(() {});
                          },
                          child: Icon((_obscurePassword)
                              ? Icons.visibility_off
                              : Icons.visibility),
                        )),
                  ),
                ),
                if (!isLogin)
                  SizedBox(height: ScreenSizeConfig.safeBlockVertical * 2),
                if (!isLogin)
                  Container(
                    height: 50,
                    child: new TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: _nameController,
                        validator: (value) =>
                            value.isEmpty ? 'Name Required' : null,
                        style: TextStyle(decoration: TextDecoration.none),
                        decoration: fieldDecoration(
                            "Full Name", Icon(MdiIcons.accountTie))),
                  ),
                Opacity(
                  opacity: (1 - _initialAnimation.value),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenSizeConfig.safeBlockVertical * 1),
                      Align(
                          alignment: Alignment.centerRight,
                          child: IgnorePointer(
                            ignoring: !isLogin,
                            child: InkWell(
                              onTap: () {
                                clearVariablesData();
                                Navigator.of(context).push(CupertinoPageRoute(
                                  builder: (context) => ResetPasswordPage(),
                                ));
                              },
                              child: Text(
                                'Forgot Password?',
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 13,
                                    color: Colors.blue),
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
                SizedBox(height: ScreenSizeConfig.safeBlockVertical * 2),
                FlatButton(
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 70),
                  onPressed: () async {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (_form.currentState.validate())
                      loginInUser(
                          email: _emailAddressController.text,
                          password: _passwordController.text,
                          isLogin: isLogin,
                          provider: 'email');
                  },
                  child: Stack(
                    children: [
                      Opacity(
                        opacity: 1 - _initialAnimation.value,
                        child: Transform.translate(
                          offset: Offset(
                              0,
                              -(_initialAnimation.value *
                                  ((ScreenSizeConfig.blockSizeVertical * 5) /
                                      1.5))),
                          child: Text(
                            'Login',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      Opacity(
                        opacity: 1 - _finalAnimation.value,
                        child: Transform.translate(
                          offset: Offset(
                              0,
                              (_finalAnimation.value *
                                  ((ScreenSizeConfig.blockSizeVertical * 5) /
                                      1.5))),
                          child: Text(
                            'Sign Up',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  color: Theme.of(context).accentColor,
                  shape: StadiumBorder(),
                ),
                SizedBox(height: ScreenSizeConfig.safeBlockVertical * 1),
                Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Divider(
                        height: 1,
                        color: Theme.of(context).hintColor,
                        thickness: 1,
                      ),
                    ),
                    SizedBox(width: 5),
                    Center(
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Opacity(
                            opacity: 1 - _initialAnimation.value,
                            child: Text("Login Using",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Theme.of(context).hintColor,
                                )),
                          ),
                          Opacity(
                            opacity: _initialAnimation.value,
                            child: Text("Sign Up Using",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Theme.of(context).hintColor,
                                )),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      child: Divider(
                        height: 1,
                        color: Theme.of(context).hintColor,
                        thickness: 1,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: ScreenSizeConfig.safeBlockVertical * 2,
                ),
                socialMediaAuthentication(),
                SizedBox(height: ScreenSizeConfig.safeBlockVertical),
              ],
            ),
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
          child: Container(
              height: 50,
              padding: EdgeInsets.all(8),
              width: double.infinity,
              color: Theme.of(context).accentColor,
              child: Center(
                  child: InkWell(
                onTap: () {
                  isLogin = !isLogin;
                  clearVariablesData();
                  if (!isLogin)
                    _controller.forward();
                  else
                    _controller.reverse();
                  setState(() {});
                },
                child: Stack(
                  children: [
                    Opacity(
                      opacity: (1 - (_initialAnimation.value)),
                      child: Transform.translate(
                        offset: Offset(
                            0,
                            -_initialAnimation.value *
                                ((ScreenSizeConfig.blockSizeVertical * 5) /
                                    1.5)),
                        child: Text(
                          "Don't have an account?",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Opacity(
                      opacity: (1 - _finalAnimation.value),
                      child: Transform.translate(
                        offset: Offset(
                            0,
                            (_finalAnimation.value *
                                ((ScreenSizeConfig.blockSizeVertical * 5) /
                                    1.5))),
                        child: Text(
                          "Already have an account?",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ))),
        ),
      ],
    );
  }

  loginInUser(
      {String email,
      String password,
      String provider,
      bool isLogin = false}) async {
    ProgressDialog dialog = ProgressDialog(context,
        isDismissible: false, type: ProgressDialogType.Normal);
    dialog.style(message: "Authenticating User");
    await dialog.show();
    try {
      UserModel callback;
      switch (provider) {
        case "email":
          {
            if (isLogin)
              callback = await AuthService().userSignIn(email, password);
            else
              callback = await AuthService()
                  .userSignUp(email, password, _nameController.text);
            break;
          }
        case "fb":
          {
            callback = await AuthService().fbSignIn();
            break;
          }
        case "google":
          {
            callback = await AuthService().googleSignIn();
            break;
          }
        case "apple":
          {
            callback = await AuthService().appleSignIn();
            break;
          }
        default:
          break;
      }

      finalAuthentication(callback, isLogin, dialog);
    } on ErrorHandler catch (e) {
      await dialog.hide();
      Toast.show(e.message, context, duration: 5);
    } catch (e) {
      await dialog.hide();
      Toast.show(e.toString(), context, duration: 5);
    }
  }

  InputDecoration fieldDecoration(String hintText, Widget icon) {
    return InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 2)),
        errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 2)),
        focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 2)),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 2)),
        focusColor: Colors.black,
        hoverColor: Colors.black,
        hintText: hintText,
        suffixIcon: icon);
  }

  String validateEmail(String data) {
    if (data.trim().length > 1) return null;
    return "Email Address Required";
  }

  String validatePassword(String data) {
    if (data.trim().length > 1) return null;
    return "Password Required";
  }

  String validateConfirmPassword(String data) {
    if (data.trim().length < 1)
      return "Password Required";
    else {
      if (data.trim().compareTo(_passwordController.text) == 0)
        return null;
      else
        return "Both Password must be same";
    }
  }

  finalAuthentication(
      UserModel user, bool isLogin, ProgressDialog dialog) async {
    try {
      if (user != null) {
        if (isLogin) {
          //from rest api get name, photourl,emailid
          UserModel data = await UserService().getUserData(user.firebase_uid);
          PreferenceService().displayName = data?.name;
          String path;
          if (data != null) {
            if (data.photo_url != null) {
              if (data.photo_url.isNotEmpty)
                path = await HttpService().downloadPicture(
                    baseUrl + data?.photo_url, user.firebase_uid);
            }
            PreferenceService().photoUrl = path;
          }
        } else {
          //send user datato rest api
          PreferenceService().displayName = user.name;
          String path;
          Uint8List bytes;
          path = await HttpService()
              .downloadPicture(user.photo_url, user.firebase_uid);
          PreferenceService().photoUrl = path;
          if (path != null) bytes = await File(path.toString()).readAsBytes();
          UserService().insertUserData(user.copyWith(
              photo_url: (bytes == null) ? null : base64.encode(bytes)));
        }

        // dynamic mcqData =
        // await BookmarkService().getMcqTrackingData(user.firebase_uid);
        // PreferenceService().mcqRightAnswer =
        //     int.tryParse((mcqData['rightAnswer'] ?? 0).toString()) ?? 0;
        // PreferenceService().mcqWrongAnswer =
        //     int.tryParse((mcqData['wrongAnswer'] ?? 0).toString()) ?? 0;
        // PreferenceService().totalTestGiven =
        //     int.tryParse((mcqData['total_test_given'] ?? 0).toString()) ?? 0;
        PreferenceService().emailId = user.email_id;
        PreferenceService().provider = user.provider;
        PreferenceService().userUid = user.firebase_uid;
        await dialog.hide();
        Navigator.of(context).pushReplacement(CupertinoPageRoute(
          builder: (context) => MainPage(),
        ));
      } else {
        await dialog.hide();
        Toast.show('There was a problem signing in.', context, duration: 5);
      }
    } catch (e) {
      print(e);
      await dialog.hide();
      Toast.show('There was a problem signing in.', context, duration: 5);
    }
  }

  socialMediaAuthentication() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        InkWell(
          onTap: () async {
            loginInUser(provider: 'google');
          },
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 1, color: Colors.black, offset: Offset(2, 1))
                ]),
            child: Image.asset(
              "assets/images/google.png",
              alignment: Alignment.center,
              height: 32,
              width: 32,
              fit: BoxFit.cover,
            ),
          ),
        ),
        // if (Platform.isIOS)
        //   InkWell(
        //     child: Container(
        //       padding: EdgeInsets.all(8),
        //       decoration: BoxDecoration(
        //           shape: BoxShape.circle,
        //           color: Colors.white,
        //           boxShadow: [
        //             BoxShadow(
        //                 blurRadius: 1,
        //                 color: Colors.black,
        //                 offset: Offset(2, 1))
        //           ]),
        //       child: Icon(
        //         MdiIcons.apple,
        //         size: 32,
        //         color: Colors.black.withOpacity(0.7),
        //       ),
        //     ),
        //     onTap: () async {
        //       loginInUser(provider: 'apple');
        //     },
        //   ),
        InkWell(
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 1, color: Colors.black, offset: Offset(2, 1))
                ]),
            child: Image.asset(
              "assets/images/fb.png",
              alignment: Alignment.center,
              height: 32,
              width: 32,
              fit: BoxFit.cover,
            ),
          ),
          onTap: () async {
            loginInUser(provider: 'fb');
          },
        ),
      ],
    );
  }

  // signInWithEmail(String _emailId, String _password) async {
  //   try {
  //     UserModel user = await AuthService().userSignIn(_emailId, _password);
  //     if (user != null) {
  //       //shared prefss
  //     }
  //   } on ErrorHandler catch (e) {
  //     Toast.show(e.message, context, duration: 6);
  //   }
  // }

  // signUpWithEmail(String _emailId, String _password) async {
  //   try {
  //     FirebaseUser user = await AuthService().userSignUp(_emailId, _password);
  //     if (user != null) {
  //       //shared prefss
  //     }
  //   } catch (e) {
  //     PlatformException exp = e;
  //     showSnackBar(exp.message);
  //   }
  // }

  showSnackBar(String message) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }
}
