import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/errorHandler.dart';
import 'package:synapse/service/authservice.dart';
import 'package:toast/toast.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  TextEditingController _resetEmailAddressController = TextEditingController();
  TextEditingController verificationCode = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> _form1 = GlobalKey();
  bool isEmailSent = false;
  GlobalKey<ScaffoldState> _skey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _skey,
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Center(
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(
                    vertical: ScreenSizeConfig.safeBlockVertical * 5,
                    horizontal: ScreenSizeConfig.safeBlockHorizontal * 8),
                margin: EdgeInsets.symmetric(
                    vertical: ScreenSizeConfig.safeBlockVertical * 20,
                    horizontal: ScreenSizeConfig.safeBlockHorizontal * 14),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white.withOpacity(0.6),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(
                  ScreenSizeConfig.blockSizeVertical * 5,
                  ScreenSizeConfig.blockSizeVertical * 23,
                  ScreenSizeConfig.blockSizeVertical * 5,
                  ScreenSizeConfig.blockSizeVertical * 4,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        offset: Offset(0, 8),
                        blurRadius: 10)
                  ],
                ),
                child: formWIdget(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget formWIdget() {
    return Form(
      key: _form1,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Text(
                'Reset Password',
                style: TextStyle(fontSize: 20, color: appColor),
              ),
            ),
            SizedBox(height: ScreenSizeConfig.safeBlockVertical * 5),
            if (!isEmailSent)
              Container(
                height: 50,
                child: new TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: _resetEmailAddressController,
                    validator: validateEmail,
                    style: TextStyle(decoration: TextDecoration.none),
                    decoration:
                        fieldDecoration("Email address", Icon(Icons.email))),
              ),
            if (isEmailSent)
              Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    child: new TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: passwordController,
                        validator: validatePassword,
                        style: TextStyle(decoration: TextDecoration.none),
                        decoration:
                            fieldDecoration("New Password", Icon(Icons.email))),
                  ),
                  SizedBox(height: ScreenSizeConfig.safeBlockVertical * 3),
                  Container(
                    height: 50,
                    child: new TextFormField(
                        controller: verificationCode,
                        validator: (value) =>
                            value.trim().isEmpty ? 'Reset Code Required' : null,
                        style: TextStyle(decoration: TextDecoration.none),
                        decoration: InputDecoration(
                          suffixIcon: Icon(Icons.vpn_key),
                          enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 2)),
                          errorBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2)),
                          focusedErrorBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2)),
                          focusColor: Colors.black,
                          hoverColor: Colors.black,
                        )),
                  ),
                ],
              ),
            SizedBox(height: ScreenSizeConfig.safeBlockVertical * 5),
            // Align(
            //   alignment:
            //       (!isEmailSent) ? Alignment.bottomRight : Alignment.bottomLeft,
            //   child: InkWell(
            //     onTap: () async {
            //       FocusScope.of(context).requestFocus(new FocusNode());
            //       if (isEmailSent) {
            //         isEmailSent = false;
            //         setState(() {});
            //       } else if (_form1.currentState.validate()) {
            //         try {
            //           await AuthService()
            //               .sendResetCode(_resetEmailAddressController.text);
            //           Toast.show('Code has been sent to you mail', context);
            //           isEmailSent = true;
            //           setState(() {});
            //         } on ErrorHandler catch (e) {
            //           Toast.show(e.message, context, duration: 5);
            //         }
            //       }
            //     },
            //     child: CircleAvatar(
            //       backgroundColor: appColorDark,
            //       child: Center(
            //         child: Icon(
            //           isEmailSent ? Icons.arrow_back : Icons.arrow_forward,
            //           color: Colors.white,
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(height: ScreenSizeConfig.safeBlockVertical * 2),
            Center(
              child: FlatButton(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 35),
                onPressed: () async {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  if (_form1.currentState.validate())
                    try {
                      await AuthService()
                          .sendResetCode(_resetEmailAddressController.text);
                      // await AuthService().resetPassword(
                      //     verificationCode.text, passwordController.text);
                    } on ErrorHandler catch (e) {
                      Toast.show(e.message, context, duration: 5);
                    } catch (e) {
                      Toast.show(e, context, duration: 5);
                    }
                },
                child: Text(
                  'Reset Password',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                color: Theme.of(context).accentColor,
                shape: StadiumBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  InputDecoration fieldDecoration(String hintText, Widget icon) {
    return InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 2)),
        errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 2)),
        focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 2)),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 2)),
        focusColor: Colors.black,
        hoverColor: Colors.black,
        hintText: hintText,
        suffixIcon: icon);
  }

  String validateEmail(String data) {
    if (data.trim().length > 1) return null;
    return "Email Address Required";
  }

  String validatePassword(String data) {
    if (data.trim().length > 1) return null;
    return "Password Required";
  }
}
