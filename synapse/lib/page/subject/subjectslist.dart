import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/subject.dart';
import 'package:synapse/page/subject/subjectHome.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/widget/synapseappbar.dart';

class SubjectListPage extends StatelessWidget {
  final int topicId;
  final String name;
  const SubjectListPage({Key key, this.topicId, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SynapseAppBar(
        title: (name),
        centetTile: false,
      ),
      body: FutureBuilder<List<Subject>>(
        future: ChapterService().getSubjectbyId(topicId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(5, 10, 5, 0),
              child: GridView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: snapshot.data.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 6),
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(13.0),
                  child: InkWell(
                    splashColor: Colors.transparent,
                    focusColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Navigator.of(context).push(CupertinoPageRoute(
                        builder: (context) => SubjectHomePage(
                          subject: snapshot.data[index],
                          topicId: topicId,
                        ),
                      ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.2),
                                spreadRadius: 3,
                                offset: Offset(0, 2),
                                blurRadius: 13)
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 15),
                        child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                snapshot.data[index].name,
                                style: TextStyle(fontSize: 15),
                              ),
                              SizedBox(
                                height: 7,
                              ),
                              Expanded(
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        child: Container(
                                      height: 1,
                                      width: 1,
                                    )),
                                    Flexible(
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: snapshot.data[index].photourl ==
                                                null
                                            ? Image.asset(
                                                imagePlaceholder,
                                                fit: BoxFit.fill,
                                              )
                                            : snapshot.data[index].photourl
                                                    .isEmpty
                                                ? Image.asset(
                                                    imagePlaceholder,
                                                    fit: BoxFit.fill,
                                                  )
                                                : CachedNetworkImage(
                                                    imageUrl: imageBaseUrl +
                                                        snapshot.data[index]
                                                            .photourl,
                                                    width: 200,
                                                    height: 200,
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Image.asset(
                                                      imagePlaceholder,
                                                      fit: BoxFit.fill,
                                                    ),
                                                    fit: BoxFit.contain,
                                                  ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ]),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
          if (snapshot.hasError) {
            Center(
              child: Text('Error Fetching Data!'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
