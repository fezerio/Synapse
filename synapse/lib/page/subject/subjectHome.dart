import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/subject.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/widget/chapterWidget.dart';
import 'package:synapse/widget/synapseappbar.dart';

class SubjectHomePage extends StatefulWidget {
  final Subject subject;
  final int topicId;

  const SubjectHomePage({Key key, this.subject, this.topicId})
      : super(key: key);
  @override
  _SubjectHomePageState createState() => _SubjectHomePageState();
}

class _SubjectHomePageState extends State<SubjectHomePage>
    with TickerProviderStateMixin {
  TabController controller;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  Map<String, List<Chapter>> completed, allData, incomplete;
  @override
  void initState() {
    super.initState();
    ChapterService().getChapterList(2, 4);
    controller = TabController(length: 3, vsync: this, initialIndex: 1);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      getData();
    });
  }

  Map<String, List<Chapter>> groupDataByDate(List<Chapter> chapter) {
    return chapter.groupBy((e) => e.live_date);
  }

  Future<void> getData() async {
    final data = (await ChapterService()
            .getChapterList(widget.topicId, widget.subject.id))
        .where((element) => element.no_of_questions > 0)
        .toList();
    completed =
        groupDataByDate(data.where((element) => element.completed).toList());
    incomplete = groupDataByDate(
        data.where((element) => element.not_completed).toList());
    allData = groupDataByDate(data);
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SynapseAppBar(
        centetTile: false,
        title: widget.subject?.name ?? '',
      ),
      body: SmartRefresher(
        physics: BouncingScrollPhysics(),
        controller: refreshController,
        onRefresh: () async {
          await getData();
          refreshController.refreshCompleted();
        },
        child: Column(
          children: <Widget>[
            Material(
              elevation: 15,
              shadowColor: Colors.black,
              color: Colors.red,
              child: Container(
                padding: const EdgeInsets.only(top: 20, bottom: 2),
                color: appColor,
                child: TabBar(
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Colors.white,
                  indicatorWeight: 4,
                  labelPadding: const EdgeInsets.only(bottom: 3),
                  tabs: [
                    Text(
                      '   All Test   ',
                    ),
                    Text(
                      '   Not Completed   ',
                    ),
                    Text(
                      '   Completed   ',
                    ),
                  ],
                  controller: controller,
                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                children: [
                  buildBody(0),
                  buildBody(1),
                  buildBody(2),
                ],
                controller: controller,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildBody(int index1) {
    Map<String, List<Chapter>> chapterData;
    if (index1 == 0)
      chapterData = allData;
    else if (index1 == 2)
      chapterData = completed;
    else
      chapterData = incomplete;

    if (chapterData == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if (chapterData.isEmpty) {
      return emptyDataWidget();
    } else
      return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22),
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  final chapters = chapterData.values.toList()[index];
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          DateFormat('dd MMM yyyy').format(
                            DateTime.parse(
                                chapterData.keys.toList()[index].toString()),
                          ),
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.7),
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: chapters.length,
                          itemBuilder: (context, index) => Padding(
                            padding: const EdgeInsets.symmetric(vertical: 9),
                            child: ChapterWidget(
                              onNavigate: () {
                                setState(() {
                                  completed = null;
                                  allData = null;
                                  incomplete = null;
                                });
                                getData();
                              },
                              chapter: chapters[index],
                              title: widget.subject.name,
                              isCompleted: index1 == 2,
                              // isMockTest: true,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: chapterData.length,
              ),
            ),
          ],
        ),
      );
    // ListView.builder(
    //   itemBuilder: (context, index) => ChapterWidget(
    //     chapter: data[index],
    //     isCompleted: index1 == 0,
    //     title: widget.subject.name,
    //   ),
    //   itemCount: data.length,
    //   physics: BouncingScrollPhysics(),
    // );
  }
}
