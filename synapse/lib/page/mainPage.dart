import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/SummaryInfo.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/discussionmodel.dart';
import 'package:synapse/model/mcqQuestion.dart';
import 'package:synapse/page/askAnything/postPage.dart';
import 'package:synapse/page/leaderboard.dart';
import 'package:synapse/provider/chapterProvider.dart';
import 'package:synapse/page/mockTestPage.dart';
import 'package:synapse/provider/leaderboardProvider.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/chapterservice.dart';
import 'package:synapse/service/notificationService.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:synapse/page/homepage/homepage.dart';
import 'package:synapse/page/dailyTestPage.dart';
import 'package:synapse/service/userService.dart';
import 'package:synapse/widget/customNavyBar.dart';
import 'package:synapse/widget/synapseappbar.dart';
import 'package:synapse/widget/synapseappdrawer.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // int index = 0;
  ValueNotifier<int> indexValue = ValueNotifier(0);
  // StreamController<LeaderBoardProvider> controller =
  //     StreamController<LeaderBoardProvider>();
  // Timer timer;
  ValueNotifier<LeaderBoardProvider> leaderBoardData = ValueNotifier(null);
  @override
  void dispose() {
    // controller?.close();
    // timer?.cancel();
    super.dispose();
  }

  // startTimer() {
  //   timer = Timer.periodic(Duration(seconds: 1), (timer) {
  //     LeaderBoardService().getLeaderBoardData(b: timer.tick).then((value) {
  //       controller.add(value);
  //     });
  //   });
  // }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(MainPage oldWidget) {
    // timer.cancel();
    super.didUpdateWidget(oldWidget);
  }

  ValueNotifier<List<DiscussionModel>> modelData = ValueNotifier(null);
  @override
  void initState() {
    // startTimer();

    MessagingService().init(context);
    // FirebaseMessaging firebaseMessaging = FirebaseMessaging();
    // firebaseMessaging.autoInitEnabled().then((value) {
    //   // if (!value) {
    //   // firebaseMessaging.subscribeToTopic('users');
    //   firebaseMessaging.setAutoInitEnabled(true);
    //   firebaseMessaging.requestNotificationPermissions();
    //   firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(
    //       sound: true,
    //       badge: true,
    //       alert: true,
    //     ),
    //   );
    //   // firebaseMessaging.getToken().then((value) {});
    //   // }
    // });
    ChapterService().notificationCenter().then((value) {
      if (value == null) return;
      mockTestNotifier.value = value;
    });
    UserService().leaderBoardData().then((value) {
      if (value != null) {
        leaderBoardData.value = value;
      }
    });
    super.initState();
  }

  ValueNotifier<List<Chapter>> mockTestNotifier = ValueNotifier(null);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        FutureProvider<List<MCQQuestion>>.value(
            value: QuestionService().getMCQQuestion()),
        FutureProvider<SummaryInfo>.value(
            value: BookmarkService()
                .getMcqTrackingData(PreferenceService().userUid)),
        FutureProvider<List<Chapter>>.value(
            value: ChapterService().getSuggestedTest()),
        FutureProvider<ChapterProvider>.value(
            value: ChapterService().fetchTestChapters()),
      ],
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: SynapseAppBar(
          title: (appName),
          actions: <Widget>[
            // FutureBuilder<List<Chapter>>(
            //   future: ChapterService().notificationCenter(),
            //   builder: (context, snapshot) => PopupMenuButton<int>(
            //     shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.only(
            //       bottomLeft: Radius.circular(40),
            //       bottomRight: Radius.circular(40),
            //       topLeft: Radius.circular(40),
            //     )),
            //     icon: Icon((snapshot?.data ?? []).length > 0
            //         ? Icons.notifications_active
            //         : Icons.notifications_none),
            //     padding: const EdgeInsets.all(0),
            //     itemBuilder: (context) => (snapshot?.data ?? []).map((e) {
            //       final daysLeft = DateTime.parse(e.live_date)
            //           .difference(DateTime(DateTime.now().year,
            //               DateTime.now().month, DateTime.now().toUtc().day))
            //           .inDays;
            //       return PopupMenuItem<int>(
            //         value: e.id,
            //         child: ListTile(
            //           title: Text(
            //             e.name,
            //             maxLines: 2,
            //             overflow: TextOverflow.ellipsis,
            //           ),
            //           subtitle: daysLeft <= 0
            //               ? LiveAnimationFlip()
            //               : Text(
            //                   daysLeft.toString() +
            //                       (daysLeft == 1 ? ' Day ' : ' Days ') +
            //                       'Left',
            //                   style: TextStyle(
            //                     color: appColor,
            //                     fontWeight: FontWeight.bold,
            //                     fontSize: 13,
            //                   ),
            //                 ),
            //         ),
            //       );
            //     }).toList(),
            //     tooltip: 'Notification',
            //     onSelected: (value) {
            //       if (indexValue.value != 2) {
            //         final date = (snapshot?.data ?? [])
            //             .where((element) => element.id == value)
            //             .toList()[0]
            //             .live_date;
            //         final daysLeft = DateTime.parse(date)
            //             .difference(DateTime(DateTime.now().year,
            //                 DateTime.now().month, DateTime.now().toUtc().day))
            //             .inDays;
            //         if (daysLeft <= 0) {
            //           // setState(() {
            //           indexValue.value = 2;
            //           // });
            //         }
            //       }
            //     },
            //     // child: Icon(Icons.notifications_active),
            //   ),
            // )
          ],
        ),
        drawer: SynapseAppDrawer(),
        body: ValueListenableBuilder(
          valueListenable: indexValue,
          builder: (context, value, child) => getRespectiveFieldPage(value),
        ),
        bottomNavigationBar: bottomNavBar(),
      ),
    );
  }

  Widget bottomNavBar() {
    return ValueListenableBuilder<int>(
      valueListenable: indexValue,
      builder: (context, value, child) => CustomNavBar(
        items: [
          navBarItem(icons: MdiIcons.homeOutline, title: 'Home'),
          navBarItem(icons: MdiIcons.gestureDoubleTap, title: 'Daily Dose'),
          navBarItem(icons: MdiIcons.clipboardOutline, title: 'Mock Test'),
          navBarItem(icons: Icons.help_outline, title: 'Ask Anything'),
          navBarItem(icons: Icons.supervised_user_circle, title: 'LeaderBoard'),
        ],
        currentIndex: value,
        onItemSelected: (value) {
          // setState(() {
          indexValue.value = value;
          // });
        },
      ),
    );
  }

  CustomNavBarItem navBarItem({IconData icons, String title}) {
    return CustomNavBarItem(
        icon: Icon(
          icons,
        ),
        title: Text(
          title,
          maxLines: 2,
          textAlign: TextAlign.center,
        ),
        activeColor: Colors.white,
        inactiveColor: Color(0xff232f34));
  }

  Widget getRespectiveFieldPage(int index) {
    switch (index) {
      case 0:
        return Consumer<List<MCQQuestion>>(
          builder: (context, value, child) =>
              Consumer<List<Chapter>>(builder: (context, value1, child) {
            return ValueListenableBuilder<List<Chapter>>(
              valueListenable: mockTestNotifier,
              builder: (context, value11, child) => HomePage(
                summaryInfo: Provider.of<SummaryInfo>(context, listen: false),
                questions: value,
                mockTest: value11,
                suggestedTest: value1,
              ),
            );
          }),
        );
      case 1:
        return Consumer<ChapterProvider>(
          builder: (context, value, child) => DailyTestPage(
            chapters: value?.dailyTest,
          ),
        );
      case 2:
        return Consumer<ChapterProvider>(
          builder: (context, value, child) => MockTestPage(
            chapters: value?.mockTest,
          ),
        );
      case 3:
        return ValueListenableBuilder(
          valueListenable: modelData,
          builder: (context, value, child) => PostPage(
            modelData: value,
            onDataUpdate: (data) {
              modelData.value = data;
            },
          ),
        );
      case 4:
        return ValueListenableProvider<LeaderBoardProvider>.value(
          value: leaderBoardData,
          child: LeaderBoard(
            onDataChange: (value) {
              leaderBoardData.value = value;
            },
          ),
        );

      default:
        return Consumer<List<MCQQuestion>>(
          builder: (context, value, child) =>
              Consumer<List<Chapter>>(builder: (context, value1, child) {
            return ValueListenableBuilder<List<Chapter>>(
              valueListenable: mockTestNotifier,
              builder: (context, value11, child) => HomePage(
                summaryInfo: Provider.of<SummaryInfo>(context, listen: false),
                questions: value,
                mockTest: value11,
                suggestedTest: value1,
              ),
            );
          }),
        );
    }
  }
}
