import 'package:synapse/model/chapter.dart';

class ChapterProvider {
  List<Chapter> dailyTest;
  List<Chapter> mockTest;

  ChapterProvider({this.dailyTest, this.mockTest});
}
