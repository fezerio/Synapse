import 'package:synapse/model/leaderboard.dart';

class LeaderBoardProvider {
  List<LeaderBoardUserData> mockTest;
  List<LeaderBoardUserData> dailyTest;
  List<LeaderBoardUserData> subjectTest;

  LeaderBoardProvider({
    this.mockTest,
    this.dailyTest,
    this.subjectTest,
  });
}
