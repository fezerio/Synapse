import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:photo_view/photo_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:synapse/config/screen_size_config.dart';

const String appName = 'Synapse';
const Color appColor = Color(0xff0099cc);
const Color appColorDark = Color(0xff2762fe);
const String imagePlaceholder = 'assets/images/placeholder.jpg';
const String userPicturePlaceholder = 'assets/images/placeholder.png';
const String baseUrl = 'https://synapsepg.com/SynapseBackend/';
// 'http://192.168.100.27/Web/SynapseAdmin/SynapseBackend/';

///For image posted by adminSide only
const String imageBaseUrl =
// 'http://192.168.100.27/Web/SynapseAdmin/';
    'https://synapsepg.com/';
const TextStyle emptyListStyle = TextStyle(color: Colors.grey, fontSize: 25);
ThemeData themeData = ThemeData(
  scaffoldBackgroundColor: Colors.white,
  inputDecorationTheme: InputDecorationTheme(
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 2,
          style: BorderStyle.solid,
          color: Colors.grey,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          width: 2,
          style: BorderStyle.solid,
          color: Colors.grey,
        ),
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(
          width: 3,
          style: BorderStyle.solid,
          color: Colors.grey,
        ),
      )),
  buttonTheme: ButtonThemeData(
      buttonColor: appColor, textTheme: ButtonTextTheme.primary),
  appBarTheme: AppBarTheme(
    elevation: 0,
    color: appColor,
    brightness: Brightness.dark,
    actionsIconTheme: IconThemeData(color: Colors.black, size: 18),
    iconTheme: IconThemeData(color: Colors.white, size: 18),
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Colors.white),
      headline6: TextStyle(
          color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
    ),
  ),
  primaryColor: appColor,
  buttonColor: appColor,
  textTheme: TextTheme(
    headline6: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
    bodyText1: TextStyle(color: Colors.black),
    bodyText2: TextStyle(color: Colors.black, fontSize: 15),
    button: TextStyle(color: Colors.white),
  ),
);
Future<void> deleteImage(String path) async {
  if (await File(path.toString()).exists()) {
    File(path).deleteSync();
  }
}

extension Iterables<E> on Iterable<E> {
  Map<K, List<E>> groupBy<K>(K Function(E) keyFunction) => fold(
      <K, List<E>>{},
      (Map<K, List<E>> map, E element) =>
          map..putIfAbsent(keyFunction(element), () => <E>[]).add(element));
}

showPhotoView(String url, BuildContext context, bool isLocal) {
  showDialog(
    barrierDismissible: true,
    context: context,
    builder: (context) => PhotoView(
        backgroundDecoration: BoxDecoration(color: Colors.transparent),
        imageProvider: (isLocal ?? false)
            ? FileImage(File(url))
            : CachedNetworkImageProvider(
                baseUrl + url,
              )),
  );
}

displayExplanation(BuildContext context, String explanation,
    {int correctAnswer, int totalAnswer}) {
  double percent =
      (totalAnswer == 0 ? 0.0 : (correctAnswer / totalAnswer)) * 100;
  showBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (context) => ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      child: Container(
        constraints: BoxConstraints(
            maxHeight: ScreenSizeConfig.safeBlockVertical * 85,
            maxWidth: ScreenSizeConfig.blockSizeHorizontal * 100),
        width: double.maxFinite,
        color: appColor,
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 25),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Text(
                percent.toStringAsFixed(0) + ' % of user gave correct answer',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              SizedBox(
                height: 10,
              ),
              Html(
                data: explanation ?? '',
                shrinkWrap: true,
                onImageTap: (url) {
                  showDialog(
                      barrierDismissible: true,
                      context: context,
                      builder: (context) => PhotoView(
                          backgroundDecoration:
                              BoxDecoration(color: Colors.transparent),
                          imageProvider: MemoryImage(
                              base64.decode(url.split('base64,').last))));
                },
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget emptyDataWidget({String imageUrl}) {
  return Center(
    child: Image.asset(
      imageUrl ?? 'assets/images/empty-box.png',
      height: 100,
      width: 100,
      fit: BoxFit.contain,
    ),
  );
}
