import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/leaderboard.dart';

class UserCard extends StatelessWidget {
  final LeaderBoardUserData userData;
  final int rank;
  UserCard({Key key, this.userData, this.rank}) : super(key: key);

  final TextStyle textStyle =
      TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.w900);
  final TextStyle nameTextStyle = TextStyle(
    color: Colors.black.withOpacity(0.8),
    fontSize: 18,
  );
  @override
  Widget build(BuildContext context) {
    final percent = (userData.right_answer / userData.total) * 100;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 5),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 12),
          child: Row(
            children: <Widget>[
              Text(
                (rank < 10 ? '0' : '') + rank.toString(),
                style: textStyle,
              ),
              SizedBox(
                width: 8,
              ),
              CircleAvatar(
                backgroundColor: Colors.white,
                radius: 20,
                backgroundImage: userData.photo_url?.isEmpty ?? true
                    ? AssetImage(
                        'assets/images/placeholder.png',
                        // height: 45,
                        // width: 45,
                        // fit: BoxFit.contain,
                        // color: Colors.white,
                      )
                    : CachedNetworkImageProvider(
                        baseUrl + userData.photo_url,
                        // height: 45,
                        // width: 45,
                        // errorBuilder: (context, error, stackTrace) => Image.asset(
                        //   'assets/images/placeholder.png',
                        //   height: 45,
                        //   width: 45,
                        //   fit: BoxFit.contain,
                        // ),
                        // fit: BoxFit.contain,
                      ),
              ),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text(
                  userData.name.toString(),
                  style: nameTextStyle,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                percent.toStringAsFixed(1) + ' %',
                style: textStyle,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
