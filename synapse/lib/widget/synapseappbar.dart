import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';

class SynapseAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final PreferredSizeWidget bottom;
  final List<Widget> actions;
  final double elevation;
  final bool centetTile;
  final bool isExamPage;

  SynapseAppBar({
    Key key,
    this.title = appName,
    this.actions,
    this.centetTile = false,
    this.bottom,
    this.elevation = 0.0,
    this.isExamPage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      centerTitle: centetTile ?? true,
      elevation: elevation ?? 0.0,
      backgroundColor: appColor,
      automaticallyImplyLeading: false,
      leading: isExamPage
          ? Container(
              height: 1,
              width: 1,
            )
          : Builder(
              builder: (context) {
                final scaffold = Scaffold.of(context);
                final parentRoute = ModalRoute.of(context);
                Widget leading;
                if (scaffold.hasDrawer) {
                  leading = IconButton(
                    icon: const Icon(
                      Icons.menu,
                      size: 30.0,
                    ),
                    onPressed: () => scaffold.openDrawer(),
                    tooltip:
                        MaterialLocalizations.of(context).openAppDrawerTooltip,
                  );
                } else if (parentRoute.canPop)
                  leading = parentRoute is PageRoute<dynamic> &&
                          parentRoute.fullscreenDialog
                      ? const CloseButton()
                      : const BackButton();
                if (leading != null) {
                  leading = ConstrainedBox(
                    constraints:
                        const BoxConstraints.tightFor(width: kToolbarHeight),
                    child: leading,
                  );
                }
                return leading;
              },
            ),
      actions: actions,
      bottom: bottom,
    );
  }

  // sendEmail(String z1) async {
  //   Email mail = Email(body: z1, subject: "Error Exporting");
  //   await FlutterEmailSender.send(mail);
  // }

  // Future<void> exportDatabase(String z1) async {
  //   Email mail =
  //       Email(attachmentPaths: [z1], body: "Data-base", subject: "Database");
  //   await FlutterEmailSender.send(mail);
  // }

  @override
  Size get preferredSize =>
      Size.fromHeight(kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0));
}
