import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/resultAnswer.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:toast/toast.dart';

class ResultQuestionWidget extends StatefulWidget {
  final ResultAnswer question;
  final bool isLastPage;
  final bool firstPage;
  final void Function() onSubmit;
  final void Function() onPreviousPress;
  final void Function() onNextPress;
  ResultQuestionWidget({
    Key key,
    this.question,
    this.isLastPage,
    this.onSubmit,
    this.firstPage,
    this.onNextPress,
    this.onPreviousPress,
  }) : super(key: key);
  @override
  _ResultQuestionWidgetState createState() => _ResultQuestionWidgetState();
}

///choosed value: 0=null -1=incorrect 1=correct
class _ResultQuestionWidgetState extends State<ResultQuestionWidget>
    with TickerProviderStateMixin {
  List<int> choosed;
  int rightAnswer = 0;
  int totalAnswer = 0;
  @override
  void initState() {
    super.initState();
    choosed = List.filled(4, 0);
    if (widget.question.answer == widget.question.user_answer) {
      if (widget.question.answer > 0) choosed[widget.question.answer - 1] = 1;
    } else {
      if (widget.question.answer > 0) {
        choosed[widget.question.answer - 1] = 1;
      }
      if (widget.question.user_answer > 0)
        choosed[widget.question.user_answer - 1] = -1;
    }
    QuestionService()
        .getQuestionRightAnswerInfo(widget.question.id)
        .then((value) {
      rightAnswer = value['rightAnswer'];
      totalAnswer = value['totalAnswer'];
    });
  }

  bool reported = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: <Widget>[
                      ListTile(
                        title: Text(widget.question?.question.toString()),
                        contentPadding: const EdgeInsets.all(0),
                      ),
                      listWidget(
                          index: 0,
                          option: widget.question?.option_a,
                          title: 'a'),
                      listWidget(
                          index: 1,
                          option: widget.question?.option_b,
                          title: 'b'),
                      listWidget(
                          index: 2,
                          option: widget.question?.option_c,
                          title: 'c'),
                      listWidget(
                          index: 3,
                          option: widget.question?.option_d,
                          title: 'd'),
                      SizedBox(
                        height: 50,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            focusColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onTap: () {
                              displayExplanation(
                                  context, widget.question.explanation,
                                  correctAnswer: rightAnswer,
                                  totalAnswer: totalAnswer);
                            },
                            child: Text(
                              'See Explanation',
                              style: TextStyle(
                                  color: appColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  decorationStyle: TextDecorationStyle.solid),
                            ),
                          ),
                          InkWell(
                            focusColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onTap: () {
                              if (!reported) {
                                reported = true;
                                BookmarkService()
                                    .reportQuestion(widget.question.id);
                                Toast.show(
                                    'Question has been reported', context);
                              }
                            },
                            child: Text(
                              'Report',
                              style: TextStyle(
                                  color: appColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  decorationStyle: TextDecorationStyle.solid),
                            ),
                          )
                        ],
                      ),
                    ]),
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              if (!widget.firstPage)
                Align(
                  alignment: Alignment.centerRight,
                  child: arrowIcon(false),
                ),
              Expanded(
                child: Container(),
              ),
              if (!widget.isLastPage)
                Align(
                  alignment: Alignment.centerRight,
                  child: arrowIcon(true),
                ),
            ],
          ),
        ],
      ),
    );
  }

  arrowIcon(bool next) {
    return ClipRRect(
      borderRadius: next
          ? BorderRadius.horizontal(left: Radius.circular(20))
          : BorderRadius.horizontal(right: Radius.circular(20)),
      child: InkWell(
          onTap: () {
            if (next)
              widget.onNextPress();
            else
              widget.onPreviousPress();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 8),
            child: Icon(
              next ? Icons.arrow_forward : Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
            color: appColorDark,
          )),
    );
  }

  Widget listWidget({String title, int index, String option}) {
    final boxColor = (choosed[index] == 1)
        ? Colors.green
        : (choosed[index] == -1) ? Colors.red : Colors.white;
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: boxColor,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  // spreadRadius: 0,
                  offset: Offset(0, 6))
            ]),
        child: Padding(
          padding: const EdgeInsets.only(left: 25, right: 15),
          child: ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
            title: Text(option.toString(),
                style: TextStyle(
                    color: boxColor != Colors.white
                        ? Colors.white
                        : Colors.black)),
          ),
        ),
      ),
    );
  }
}
