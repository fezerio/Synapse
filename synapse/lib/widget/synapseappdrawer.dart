import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/page/aboutUsPage.dart';
import 'package:synapse/page/askAnything/postPage.dart';
import 'package:synapse/page/bookmark/bookmarklist.dart';
import 'package:synapse/page/exam/completionPage.dart';
import 'package:synapse/service/authservice.dart';
import 'package:synapse/service/preferenceservice.dart';
import 'package:synapse/service/userService.dart';
import 'package:synapse/widget/permissionrequiredDialog.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class DrawerItem {
  final String title;
  final Function onTap;
  final String linkToLaunch;

  DrawerItem({this.title = '', this.onTap, this.linkToLaunch});
}

class SynapseAppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: appColor,
                width: double.maxFinite,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 0.0, left: 8, right: 8, bottom: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () async {
                            PermissionStatus status =
                                await Permission.storage.request();
                            if (!status.isGranted) {
                              Toast.show('Permission Required', context);
                              return;
                            }
                            try {
                              ImagePicker()
                                  .getImage(source: ImageSource.gallery)
                                  .then((image) async {
                                if (image == null) return;
                                uploadImage(image);
                                Navigator.pop(context);
                              });
                            } catch (e) {
                              Toast.show(e.message ?? e, context);
                            }
                          },
                          child: FutureBuilder<bool>(
                            future:
                                File(PreferenceService().photoUrl.toString())
                                    .exists(),
                            builder: (context, snapshot) {
                              if (snapshot?.data ?? false) {
                                return CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Colors.white,
                                  foregroundColor: Colors.white,
                                  backgroundImage: FileImage(
                                    File(
                                      PreferenceService().photoUrl.toString(),
                                    ),
                                  ),
                                );
                              } else {
                                return CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Colors.white,
                                  backgroundImage: AssetImage(
                                    userPicturePlaceholder,
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Text(
                        (PreferenceService().displayName),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                          fontSize: 25.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              drawerItem(
                icon: Icons.bookmark,
                onTap: () {
                  navigator(context: context, page: BookMarkList());
                },
                title: 'Bookmarked',
              ),
              // drawerItem(
              //   icon: Icons.storage,
              //   onTap: () {
              //     navigator(context: context, page: ReportBucketList());
              //   },
              //   title: 'Bug Bucket List',
              // ),
              // drawerItem(
              //   icon: Icons.question_answer,
              //   onTap: () {
              //     navigator(context: context, page: PostPage());
              //   },
              //   title: 'Ask Anything',
              // ),
              drawerItem(
                icon: Icons.error_outline,
                onTap: () {
                  // navigator(context: context, page: ExamCompletionPage());
                },
                title: 'FAQs',
              ),
              // Divider(
              //   thickness: 0.7,
              // ),
              drawerItem(
                icon: Icons.contact_phone,
                onTap: () {
                  navigator(context: context, page: ContactUs());
                },
                title: 'Contact Us',
              ),
              drawerItem(
                icon: Icons.share,
                onTap: () {
                  Share.share(
                      '''Check out this awesome quiz app. For android: market://details?id=com.fezerio.synapse \n
                       For Ios: http://itunes.apple.com/us/app/id{com.fezerio.synapse}?mt=8''');
                },
                title: 'Share Us',
              ),
              drawerItem(
                icon: Icons.star_border,
                onTap: () {
                  if (Platform.isAndroid) {
                    launcher('market://details?id=com.fezerio.synapse');
                  } else if (Platform.isIOS) {
                    launcher(
                        'itms-apps://itunes.apple.com/app/apple-store/id1541446415?mt=8');
                  }
                },
                title: 'Rate Us',
              ),
              drawerItem(
                icon: MdiIcons.logoutVariant,
                onTap: () {
                  AuthService().logout(context);
                },
                title: 'Logout',
              ),
            ],
          )
        ],
      ),
    );
  }

  uploadImage(PickedFile image) async {
    String uid = PreferenceService().userUid;
    Directory dir = await getApplicationDocumentsDirectory();
    String ext = image.path.split('/').last.split('.').last;
    deleteImage(PreferenceService().photoUrl);
    String imageDir = dir.path +
        '/user_$uid' +
        DateTime.now().toUtc().toIso8601String() +
        "." +
        ext;
    await File(image.path).copy(imageDir);
    PreferenceService().photoUrl = imageDir;
    final data = await File(imageDir).readAsBytes();
    UserService().updateUserProfilePic(base64Encode(data));
  }

  Widget drawerItem({String title, Function onTap, IconData icon}) {
    return ListTile(
      leading: Icon(
        icon,
        color: appColor,
      ),
      title: Text(title.toUpperCase()),
      onTap: onTap,
    );
  }

  navigator({BuildContext context, Widget page}) {
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (context) => page,
    ));
  }

  launcher(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    }
  }
}
