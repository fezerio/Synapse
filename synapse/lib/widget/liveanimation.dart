import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LiveAnimationFlip extends StatefulWidget {
  @override
  _LiveAnimationFlipState createState() => _LiveAnimationFlipState();
}

class _LiveAnimationFlipState extends State<LiveAnimationFlip>
    with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    controller = AnimationController(
        vsync: this,
        duration: Duration(seconds: 2),
        lowerBound: 6,
        upperBound: 9);
    controller.repeat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) => Text(
        'LIVE NOW',
        style: TextStyle(
            color: Colors.red[controller.value.toInt() * 100],
            fontWeight:
                controller.value < 7.5 ? FontWeight.w600 : FontWeight.w800,
            fontSize: 12,
            wordSpacing: 2,
            letterSpacing: 1),
      ),
    );
  }
}
