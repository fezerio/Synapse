import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/model/images.dart';
import 'package:synapse/model/question.dart';
import 'package:synapse/service/bookmarkService.dart';
import 'package:synapse/service/questionservice.dart';
import 'package:toast/toast.dart';

class InteractiveQuestion extends StatefulWidget {
  final Question question;
  final bool revealAnswer;
  final bool isLastPage;
  final bool isTest;
  final bool timeEnded;
  final bool showButtons;
  final bool isMockTest;
  final Map<String, int> revealAnswerData;
  // final bool isSubmitted;
  final void Function(bool isTrue, int userAnswer) onSubmit;
  final void Function() onPreviousPress;
  final void Function(bool isTrue, int userAanswer) onNextPress;
  InteractiveQuestion(
      {Key key,
      this.question,
      this.showButtons = true,
      this.revealAnswerData,
      //   this.isSubmitted = false,
      this.timeEnded = false,
      this.onSubmit,
      this.isMockTest = false,
      this.revealAnswer = false,
      this.isLastPage = false,
      this.onNextPress,
      this.onPreviousPress,
      this.isTest = true})
      : super(key: key);
  @override
  _InteractiveQuestionState createState() => _InteractiveQuestionState();
}

class _InteractiveQuestionState extends State<InteractiveQuestion>
    with TickerProviderStateMixin {
  List<bool> choosed;
  // AnimationController animationController;
  // Animation animation;
  bool reported = false;
  bool submitted = false;
  int _choosedAnswerIndex;

  //means user has answered this questions and pressed next/submit
  ValueNotifier<bool> isDone = ValueNotifier(false);
  int rightAnswer = 0;
  int totalAnswer = 0;
  @override
  void initState() {
    resetList();
    if (widget.revealAnswer) {
      if (widget.revealAnswerData != null) {
        isDone.value = true;
        if ((widget.revealAnswerData['userAnswer'] - 1) > 0) {
          choosed[widget.revealAnswerData['userAnswer'] - 1] = false;
        }
        choosed[widget.revealAnswerData['answer'] - 1] = true;
      }
    }
    QuestionService()
        .getQuestionRightAnswerInfo(widget.question.id)
        .then((value) {
      rightAnswer = value['rightAnswer'];
      totalAnswer = value['totalAnswer'];
    });
    super.initState();
    // animationController =
    //     AnimationController(vsync: this, duration: Duration(seconds: 1));
    // animation = Tween(begin: 0.0, end: 1.0).animate(animationController);
  }

  @override
  void dispose() {
    // animationController?.stop();
    // animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 37, right: 30),
          child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                ListTile(
                  title: Text(
                    widget.question?.question.toString(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: FutureBuilder<List<Images>>(
                    future:
                        QuestionService().getQuestionImage(widget.question.id),
                    builder: (context, snapshot) => snapshot.hasData
                        ? Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: snapshot.data
                                    .map((e) => Padding(
                                          padding:
                                              const EdgeInsets.only(right: 8),
                                          child: InkWell(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                barrierDismissible: true,
                                                builder: (context) => PhotoView(
                                                    imageProvider:
                                                        CachedNetworkImageProvider(
                                                            imageBaseUrl +
                                                                e.image)),
                                              );
                                            },
                                            child: CachedNetworkImage(
                                              imageUrl: imageBaseUrl + e.image,
                                              height: 50,
                                              width: 50,
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Image.asset(
                                                imagePlaceholder,
                                                height: 50,
                                                width: 50,
                                              ),
                                            ),
                                          ),
                                        ))
                                    .toList(),
                              ),
                              physics: BouncingScrollPhysics(),
                            ),
                          )
                        : Container(
                            height: 1,
                            width: 1,
                          ),
                  ),
                  contentPadding: const EdgeInsets.all(0),
                ),
                SizedBox(
                  height: 10,
                ),
                listWidget(
                    index: 0, option: widget.question?.option_a, title: 'a'),
                listWidget(
                    index: 1, option: widget.question?.option_b, title: 'b'),
                listWidget(
                    index: 2, option: widget.question?.option_c, title: 'c'),
                listWidget(
                    index: 3, option: widget.question?.option_d, title: 'd'),
              ]),
        ),
        SizedBox(
          height: 12,
        ),
        if (widget.showButtons)
          if (widget.isMockTest)
            SizedBox(
              height: 12,
            ),
        if (widget.showButtons)
          if (widget.isLastPage)
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: RaisedButton(
                onPressed: () async {
                  if (isDone.value) {
                    return;
                  }
                  if (submitted) return;
                  if (widget.isTest) {
                    if (!(widget.timeEnded)) if (_choosedAnswerIndex == null) {
                      await showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (context) => AlertDialog(
                          actions: <Widget>[],
                          content: Text('Cannot submit without answer'),
                        ),
                      );
                      return;
                    }
                    // widget.onSubmit(
                    //     _choosedAnswerIndex == widget.question?.answer,
                    //     widget.question.answer);
                    // return;
                  } else {
                    if (_choosedAnswerIndex == null) {
                      await showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (context) => AlertDialog(
                          actions: <Widget>[],
                          content: Text('Choose one answer, before submitting'),
                        ),
                      );
                      return;
                    }
                  }

                  isDone.value = true;
                  submitted = true;
                  if (widget.revealAnswerData != null) {
                    widget.onSubmit(
                        _choosedAnswerIndex == widget.question?.answer,
                        _choosedAnswerIndex);
                    return;
                  }
                  if (!widget.timeEnded) {
                    if (!(widget.isMockTest ?? false)) {
                      await revealAnswer();
                      await Future.delayed(Duration(seconds: 2));
                    }
                  }
                  widget.onSubmit(
                      _choosedAnswerIndex == widget.question?.answer,
                      _choosedAnswerIndex);
                },
                color: appColor,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 17),
                  child: Text(
                    'Submit',
                    style: TextStyle(
                        color: Colors.white, letterSpacing: 1, fontSize: 16),
                  ),
                ),
              ),
            )
          else
            arrowIcon(true),
        if (widget.showButtons)
          if (!widget.isMockTest)
            SizedBox(
              height: 25,
            ),
        if (widget.showButtons)
          if (!widget.isMockTest)
            Padding(
              padding: const EdgeInsets.only(left: 37, right: 37),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    focusColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      // print(widget.question.explanation);
                      displayExplanation(context, widget.question.explanation,
                          correctAnswer: rightAnswer, totalAnswer: totalAnswer);
                    },
                    child: Text(
                      'See Explanation',
                      style: TextStyle(
                          color: appColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid),
                    ),
                  ),
                  InkWell(
                    focusColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      if (!reported) {
                        reported = true;
                        BookmarkService().reportQuestion(widget.question.id);
                        Toast.show('Question has been reported', context);
                      }
                    },
                    child: Text(
                      'Report',
                      style: TextStyle(
                          color: appColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid),
                    ),
                  )
                ],
              ),
            ),
      ],
    );
  }

  arrowIcon(bool next) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: RaisedButton(
        onPressed: () async {
          if (isDone.value) {
            return;
          }
          if (_choosedAnswerIndex == null) {
            await showDialog(
              context: context,
              barrierDismissible: true,
              builder: (context) => AlertDialog(
                actions: <Widget>[],
                content:
                    Text('Cannot proceed without answering this question.'),
              ),
            );
            return;
          }
          if (next) {
            isDone.value = true;
            if (!(widget.isMockTest ?? false)) {
              await revealAnswer();
              await Future.delayed(Duration(seconds: 2));
            }
            widget.onNextPress(_choosedAnswerIndex == widget.question?.answer,
                _choosedAnswerIndex);
          }
        },
        color: appColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 17),
          child: Text(
            'Next',
            style:
                TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          ),
        ),
      ),
    );
  }

  Future<void> revealAnswer() async {
    resetList(revealAnswer: true);
  }

  resetList({bool revealAnswer = false}) {
    choosed = List.filled(4, null, growable: false);
    if (revealAnswer ?? false) {
      if (_choosedAnswerIndex > 0) choosed[_choosedAnswerIndex - 1] = false;
      if (widget.question.answer > 0)
        choosed[widget.question.answer - 1] = true;
    }
  }

  Widget listWidget({String title, int index, String option}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: ValueListenableBuilder(
          valueListenable: isDone,
          builder: (context, value, snapshot) {
            final boxColor = choosed[index] == null
                ? Colors.white
                : choosed[index]
                    ? Colors.green
                    : value ? Colors.red : Colors.white;
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: boxColor,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 10,
                        offset: Offset(0, 6))
                  ]),
              child: Padding(
                padding: const EdgeInsets.only(left: 25, right: 15),
                child: IgnorePointer(
                  ignoring: submitted || widget.timeEnded,
                  child: InkWell(
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    focusColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      if (isDone.value) {
                        return;
                      }
                      if (choosed[index] != true) {
                        _choosedAnswerIndex = index + 1;
                        resetList();
                        choosed[index] = true;
                        setState(() {});
                      }
                    },
                    child: ListTile(
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 0.0),
                      title: Text(
                        option.toString(),
                        style: TextStyle(
                            color: boxColor != Colors.white
                                ? Colors.white
                                : Colors.black),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
