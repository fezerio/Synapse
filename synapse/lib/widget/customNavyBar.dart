library bottom_navy_bar;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:synapse/config/constant.dart';

class CustomNavBar extends StatefulWidget {
  final int currentIndex;
  final List<CustomNavBarItem> items;
  final ValueChanged<int> onItemSelected;

  CustomNavBar(
      {Key key,
      this.currentIndex = 0,
      @required this.items,
      @required this.onItemSelected});

  @override
  _CustomNavBarState createState() {
    return _CustomNavBarState();
  }
}

class _CustomNavBarState extends State<CustomNavBar> {
  Widget _buildItem(CustomNavBarItem item, bool isSelected, int index) {
    return InkWell(
      onTap: () {
        widget.onItemSelected(index);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            item.icon.icon,
            color: Colors.white.withOpacity(isSelected ? 1.0 : 0.8),
            size: 26,
          ),
          isSelected
              ? Text(item.title.data,
                  style: TextStyle(
                      color: Colors.white.withOpacity(isSelected ? 1.0 : 0.8),
                      fontSize: 14,
                      fontWeight:
                          isSelected ? FontWeight.bold : FontWeight.normal))
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      color: appColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: widget.items
            .map((e) => _buildItem(
                e,
                widget.currentIndex == widget.items.indexOf(e),
                widget.items.indexOf(e)))
            .toList(),
      ),
    );
  }
}

class CustomNavBarItem {
  final Icon icon;
  final Text title;
  final Color activeColor;
  final Color inactiveColor;

  CustomNavBarItem(
      {@required this.icon,
      @required this.title,
      this.activeColor = Colors.blue,
      this.inactiveColor}) {
    assert(icon != null);
    assert(title != null);
  }
}
