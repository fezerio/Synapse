import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/page/exam/examHomePage.dart';
import 'package:synapse/page/exam/resultAnswers.dart';
import 'package:synapse/page/exam/resultPage.dart';

class ChapterWidget extends StatelessWidget {
  final Chapter chapter;
  final String title;
  final bool isCompleted;
  final bool isMockTest;
  final Function onNavigate;

  const ChapterWidget(
      {Key key,
      this.chapter,
      this.onNavigate,
      this.isCompleted = false,
      this.title,
      this.isMockTest: false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(CupertinoPageRoute(
          builder: (context) => chapter.completed ?? isCompleted
              ? ResultPage(
                  chapterName: chapter.name,
                  chapterId: chapter.id,
                  isMockTest: isMockTest,
                  timer: chapter.timer,
                )
              : ExamPage(
                  title: title,
                  chapterName: chapter.name,
                  time: chapter.timer,
                  chapterId: chapter.id,
                  isMockTest: isMockTest,
                ),
        ))
            .then((value) {
          if (onNavigate != null) onNavigate();
        });
      },
      child: Container(
        padding: const EdgeInsets.all(22),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10,
                  // spreadRadius: 0,
                  offset: Offset(0, 6))
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Text(
                chapter?.name,
                style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(((chapter?.timer ?? 0) / 3600).toStringAsFixed(1) + ' hr')
          ],
        ),
      ),
    );
  }
}
