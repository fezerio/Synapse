import 'dart:async';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:synapse/config/constant.dart';

class TimerWidget extends StatefulWidget {
  final int duration;
  final Function() onCountDownFinish;
  final bool isExamTimer;
  final bool isMCQ;

  TimerWidget(
      {Key key,
      this.duration,
      this.isMCQ = true,
      this.onCountDownFinish,
      this.isExamTimer = false});

  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> with WidgetsBindingObserver {
  Timer timer;
  int countDown;
  AppLifecycleState lifecycleState;
  StreamController<int> streamController = StreamController();
  @override
  void initState() {
    super.initState();
    countDown = widget.duration;
    if (countDown == 0) return;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      countDown--;
      streamController.add(countDown);
      if (countDown == 0 || countDown < 0) {
        timer.cancel();
        streamController.add(0);
        streamController.close();
        widget.onCountDownFinish();
      }
    });
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    timer?.cancel();
    streamController?.close();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  int zz;
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state.index == 2) {
      setState(() {});
      {
        zz = countDown;
      }
    }
    if (state.index == 0) {
      setState(() {});
      {
        countDown = zz ?? countDown;
      }
    }

    // lifecycleState = state;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: streamController.stream,
      builder: (context, snapshot) {
        return AnimatedDefaultTextStyle(
          duration: Duration(seconds: 2),
          style: TextStyle(
              color: getColor(snapshot?.data ?? widget.duration),
              fontSize: widget.isExamTimer ? 22 : 35,
              fontWeight: FontWeight.w900),
          child: Text(secondToTimeStamp(snapshot?.data ?? widget.duration)),
        );
      },
    );
  }

  Color getColor(int value) {
    if (!widget.isMCQ) {
      if (value < 300) return Colors.red.shade900;
      if (value < (widget.duration / 2))
        return Colors.red.shade400;
      else
        return widget.isExamTimer ? Colors.white : Colors.black;
    } else {
      if (value < 10) return Colors.red.shade900;
      if (value < 15)
        return Colors.red.shade400;
      else
        return appColor;
    }
  }

  String secondToTimeStamp(int value) {
    if (value < 0 || value == 0) {
      return '00:00:00';
    }
    int hr = (value / 3600).floor();
    int min = ((value % 3600) / 60).floor();
    int second = (value % 3600) % 60.floor();
    // if (widget.isExamTimer)
    //   return (hr < 10 ? '0' : '') +
    //       hr.toString() +
    //       ':' +
    //       (min < 10 ? '0' : '') +
    //       min.toString();
    // else
    return (hr < 10 ? '0' : '') +
        hr.toString() +
        ':' +
        (min < 10 ? '0' : '') +
        min.toString() +
        ':' +
        (second < 10 ? '0' : '') +
        second.toString();
  }
}
