import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synapse/config/constant.dart';

class ReportWidget extends StatefulWidget {
  final Function onSubmit;

  const ReportWidget({Key key, this.onSubmit}) : super(key: key);
  @override
  _ReportWidgetState createState() => _ReportWidgetState();
}

class _ReportWidgetState extends State<ReportWidget> {
  TextEditingController _reportController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Dialog(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 45, horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Report a Problem',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                controller: _reportController,
                minLines: 4,
                maxLines: 8,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2)),
                  errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red, width: 2)),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2)),
                  focusColor: Colors.black,
                  hoverColor: Colors.black,
                  hintText: 'Enter details here',
                ),
                validator: (value) => _reportController.text.isEmpty
                    ? 'This field cannot be empty'
                    : null,
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: widget.onSubmit,
                color: appColorDark,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 18),
                  child: Text(
                    'Submit',
                    style: TextStyle(
                        color: Colors.white, letterSpacing: 1, fontSize: 16),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
