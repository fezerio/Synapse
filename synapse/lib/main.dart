import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/config/screen_size_config.dart';
import 'package:synapse/page/authentication/authPage.dart';
import 'package:synapse/page/mainPage.dart';

import 'package:synapse/service/preferenceservice.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  PreferenceService();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: appColor,
    ),
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: ThemeData(scaffoldBackgroundColor: appColor),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  double splashHeight;

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp().then((value) {
      if (PreferenceService.isReady.value) {
        checkUser();
      } else {
        PreferenceService.isReady.addListener(() {
          if (PreferenceService.isReady.value) checkUser();
        });
      }
    });
  }

  checkUser() {
    Future.delayed(Duration(milliseconds: 1100)).then((value) {
      final data = PreferenceService().userUid;
      if (data == null)
        navigator(context: context, page: AuthPage());
      else {
        navigator(context: context, page: MainPage());
      }
    });
  }

  navigator({BuildContext context, Widget page}) {
    Navigator.of(context).pushReplacement(CupertinoPageRoute(
      builder: (context) => page,
    ));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenSizeConfig().init(context);
    splashHeight = ScreenSizeConfig.blockSizeVertical * 70;
    return Scaffold(
      backgroundColor: appColorDark,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          RotatedBox(
              quarterTurns: 2,
              child:
                  Image.asset('assets/images/splash1.png', fit: BoxFit.cover)),
          RotatedBox(
            quarterTurns: 2,
            child: Image.asset(
              'assets/images/splash2.png',
              fit: BoxFit.cover,
            ),
          ),
          Image.asset('assets/images/splash3.png', fit: BoxFit.cover),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                appName.toUpperCase(),
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.clip,
                style: TextStyle(
                    fontFamily: 'SuezOne',
                    color: Colors.white,
                    fontSize: ScreenSizeConfig.blockSizeVertical * 8),
              ),
              Transform.scale(
                scale: 1,
                child: Text(
                  'Your Study Platform',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'LobsterTwo',
                      color: Colors.white,
                      fontSize: ScreenSizeConfig.blockSizeVertical * 4),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
