import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:synapse/page/authentication/authPage.dart';
import 'package:synapse/service/preferenceservice.dart';
import '../model/user.dart';
import 'package:synapse/model/errorHandler.dart';

class AuthService {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Dio _dio = Dio();
  final FacebookLogin _facebookLogin = FacebookLogin();

  Future<UserModel> fbSignIn() async {
    if (Platform.isAndroid)
      FacebookLogin().loginBehavior = FacebookLoginBehavior.nativeWithFallback;
    else
      FacebookLogin().loginBehavior = FacebookLoginBehavior.webViewOnly;
    final FacebookLoginResult result = await _facebookLogin.logIn(['email']);
    if (result?.status != FacebookLoginStatus.loggedIn) {
      throw ErrorHandler(
          code: 1,
          message: result.status
              .toString()
              .toString()
              .split('.')
              .last
              .toUpperCase());
    }
    AuthCredential credential = FacebookAuthProvider.credential(
      result.accessToken.token,
    );
    final token = result.accessToken.token;
    final graphResponse = await _dio.get(
        'https://graph.facebook.com/v2.12/me?fields=picture.height(200)&access_token=${token}');
    final profile = json.decode(graphResponse.data);
    try {
      final User user = (await _auth.signInWithCredential(credential)).user;
      if (user == null)
        return null;
      else
        return UserModel(
            email_id: user.email,
            name: user.displayName,
            photo_url: profile['picture']['data']['url'],
            provider: 'facebook',
            firebase_uid: user.uid);
    } on FirebaseAuthException catch (z) {
      throw ErrorHandler(code: 0, message: z.message);
    } catch (e) {
      throw ErrorHandler(code: 0, message: e);
    }
  }

  Future<UserModel> googleSignIn() async {
    {
      GoogleSignInAccount googleUser;

      googleUser = await _googleSignIn.signIn();
      if (googleUser == null) {
        throw ErrorHandler(code: 1, message: 'Sign In Aborted by User');
      }

      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      try {
        final User user = (await _auth.signInWithCredential(credential)).user;
        if (user == null)
          return null;
        else
          return UserModel(
              email_id: user.email,
              name: user.displayName,
              photo_url: user.photoURL,
              provider: 'google',
              firebase_uid: user.uid);
      } on FirebaseAuthException catch (z) {
        throw ErrorHandler(code: 0, message: z.message);
      } catch (e) {
        throw ErrorHandler(code: 0, message: e);
      }
    }
  }

  Future<UserModel> appleSignIn() async {
    {
      final nonce = _createNonce(32);
      AuthorizationCredentialAppleID nativeAppleCred;
      try {
        nativeAppleCred = Platform.isIOS
            ? await SignInWithApple.getAppleIDCredential(
                scopes: [
                  AppleIDAuthorizationScopes.email,
                  AppleIDAuthorizationScopes.fullName,
                ],
                nonce: sha256.convert(utf8.encode(nonce)).toString(),
              )
            : await SignInWithApple.getAppleIDCredential(
                scopes: [
                  AppleIDAuthorizationScopes.email,
                  AppleIDAuthorizationScopes.fullName,
                ],
                webAuthenticationOptions: WebAuthenticationOptions(
                  redirectUri: Uri.parse(
                      'https://synapse-133f3.firebaseapp.com/__/auth/handler'),
                  clientId: 'com.fezerio.synapse',
                ),
                nonce: sha256.convert(utf8.encode(nonce)).toString(),
              );
      } on SignInWithAppleAuthorizationException catch (e) {
        throw ErrorHandler(code: 0, message: e.message.toString());
      } on SignInWithAppleException catch (e) {
        throw ErrorHandler(code: 0, message: e.toString());
      } catch (e) {
        throw ErrorHandler(code: 0, message: e);
      }
      if (nativeAppleCred == null) return null;
      OAuthCredential credential = OAuthCredential(
        providerId: "apple.com", // MUST be "apple.com"
        signInMethod: "oauth", // MUST be "oauth"
        accessToken: nativeAppleCred.identityToken,
        idToken: nativeAppleCred.identityToken,
        rawNonce: nonce,
      );
      try {
        final User user = (await _auth.signInWithCredential(credential)).user;
        if (user == null)
          return null;
        else
          return UserModel(
              email_id: user.email,
              name: user.displayName,
              photo_url: user.photoURL,
              provider: 'apple',
              firebase_uid: user.uid);
      } on FirebaseAuthException catch (z) {
        throw ErrorHandler(code: 0, message: z.message);
      } catch (e) {
        throw ErrorHandler(code: 0, message: e);
      }
    }
  }

  Future<UserModel> userSignIn(String email, String password) async {
    try {
      final UserCredential user = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (user?.user == null)
        return null;
      else
        return UserModel(
            email_id: user.user.email,
            name: user.user.displayName,
            photo_url: null,
            provider: 'firebase',
            firebase_uid: user.user.uid);
    } on FirebaseException catch (z) {
      switch (z.code.toUpperCase()) {
        case "USER-NOT-FOUND":
          throw ErrorHandler(
              code: 0, message: 'User Not Found, Please sign up');
        case "WRONG-PASSWORD":
          throw ErrorHandler(
              code: 0, message: 'Invalid Password, Please Try Again');
        case "INVALID-EMAIL":
          throw ErrorHandler(code: 0, message: 'Invalid Email Address');
        default:
          throw ErrorHandler(code: 0, message: z.message);
      }
    } catch (z) {
      throw ErrorHandler(code: 0, message: z);
    }
  }

  Future<UserModel> userSignUp(
      String email, String password, String name) async {
    try {
      final UserCredential user = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      if (user?.user == null)
        return null;
      else
        return UserModel(
            email_id: user.user.email,
            name: name,
            photo_url: null,
            provider: 'firebase',
            firebase_uid: user.user.uid);
    } on FirebaseException catch (z) {
      switch (z.code.toUpperCase()) {
        case "WEAK-PASSWORD":
          throw ErrorHandler(
              code: 0,
              message:
                  'Weak Password [ Password should be at least 6 characters ]');
        case "EMAIL-ALREADY-IN-USE":
          throw ErrorHandler(
              code: 0,
              message:
                  'Email Address is already in use, Please login or reset password');
        case "INVALID-EMAIL":
          throw ErrorHandler(code: 0, message: 'Invalid Email Address');
        default:
          throw ErrorHandler(code: 0, message: z.message);
      }
    } catch (z) {
      throw ErrorHandler(code: 0, message: z);
    }
  }

  Future<void> sendResetCode(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (z) {
      switch (z.code.toUpperCase()) {
        case "USER-NOT-FOUND":
          throw ErrorHandler(
              code: 0, message: 'User not found with given email address');
        case "INVALID-EMAIL":
          throw ErrorHandler(code: 0, message: 'Invalid Email Address');
        default:
          throw ErrorHandler(code: 0, message: z.message);
      }
    } catch (e) {
      throw ErrorHandler(code: 0, message: e);
    }
  }

  Future<void> resetPassword(String code, String password) async {
    try {
      await _auth.confirmPasswordReset(
        code: code,
        newPassword: password,
      );
    } on FirebaseAuthException catch (z) {
      switch (z.code.toUpperCase()) {
        case "EXPIRED-ACTION-CODE":
          throw ErrorHandler(code: 0, message: 'Code has been Expired');
        case "INVALID-ACTION-CODE":
          throw ErrorHandler(code: 0, message: 'Invalid Code');
        case "WEAK-PASSWORD":
          throw ErrorHandler(
              code: 0,
              message:
                  'Weak Password [ Password should be at least 6 characters ]');
        default:
          throw ErrorHandler(code: 0, message: z?.message.toString());
      }
    } catch (z) {
      throw ErrorHandler(code: 0, message: z?.toString());
    }
  }

  String _createNonce(int length) {
    final random = Random();
    final charCodes = List<int>.generate(length, (_) {
      int codeUnit;

      switch (random.nextInt(3)) {
        case 0:
          codeUnit = random.nextInt(10) + 48;
          break;
        case 1:
          codeUnit = random.nextInt(26) + 65;
          break;
        case 2:
          codeUnit = random.nextInt(26) + 97;
          break;
      }

      return codeUnit;
    });

    return String.fromCharCodes(charCodes);
  }

  Future<void> logout(BuildContext context) async {
    ProgressDialog progressDialog = ProgressDialog(context);
    progressDialog.style(message: 'Logging Out');
    await progressDialog.show();
    await PreferenceService().clearPreferences();
    FirebaseMessaging firebaseMessaging = FirebaseMessaging();
    await firebaseMessaging.unsubscribeFromTopic('users');
    await firebaseMessaging.setAutoInitEnabled(false);
    await progressDialog.hide();
    Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          builder: (context) => AuthPage(),
        ),
        (route) => false);
  }
}
