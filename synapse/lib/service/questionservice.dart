import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:synapse/model/errorHandler.dart';
import 'package:synapse/model/images.dart';
import 'package:synapse/model/mcqQuestion.dart';
import 'package:synapse/model/question.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

class QuestionService {
  Future<List<MCQQuestion>> getMCQQuestion() async {
    try {
      String userId = PreferenceService().userUid;

      final response = await HttpService().post(
          'get_mcq.php',
          FormData.fromMap({
            'user_id': userId,
          }));
      List<dynamic> data = jsonDecode(response.data);
      return data.map((e) => MCQQuestion.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<void> questionSolved({
    int questionId,
    int correctAnswer,
    int userAnswer,
    int chapterId,
  }) async {
    String uid = PreferenceService().userUid;
    try {
      await HttpService().post(
          'insert_question_tracking.php',
          FormData.fromMap({
            'question_id': questionId,
            'uid': uid,
            'correct_answer': correctAnswer,
            'user_answer': userAnswer,
            'chapter_id': chapterId
          }));
    } catch (e) {
      throw (ErrorHandler(code: 2, message: "Cannot Submit at the moment"));
    }
  }

  Future<List<Question>> getExamQuestions(int chapterId) async {
    try {
      String userId = PreferenceService().userUid;
      final response = await HttpService().post(
          'get_exam_question.php',
          FormData.fromMap({
            'user_id': userId,
            'chapter_id': chapterId,
          }));
      List<dynamic> data = jsonDecode(response.data);
      return data.map((e) => Question.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<Map<String, int>> getQuestionRightAnswerInfo(int questionId) async {
    try {
      final response = await HttpService().post(
          'get_questionRightAnswerInfo.php',
          FormData.fromMap({
            'question_id': questionId,
          }));
      final data = jsonDecode(response.data);
      return {
        'rightAnswer':
            int.tryParse((data['correct_answer'] ?? 0).toString()) ?? 0,
        'totalAnswer': int.tryParse((data['total_number'] ?? 0).toString()) ?? 0
      };
    } catch (e) {
      print(e);
      return {'rightAnswer': 0, 'totalAnswer': 0};
    }
  }

  Future<List<Images>> getQuestionImage(int questionId) async {
    try {
      final response = await HttpService().post(
          'get_questionImage.php',
          FormData.fromMap({
            'question_id': questionId,
          }));
      final List<dynamic> data = jsonDecode(response.data);
      return data.map((e) => Images.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }
}
