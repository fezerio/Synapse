import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:synapse/model/discussionmodel.dart';
import 'package:synapse/model/discussionpost.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

class DiscussionService {
  Future<List<DiscussionModel>> getDiscussionPost() async {
    try {
      final data = await HttpService().getService('get_discussion_post.php');
      List<dynamic> response = jsonDecode(data.data);

      return response.map((e) => DiscussionModel.fromMap(e)).toList();
    } catch (e) {
      print(e);

      return [];
    }
  }

  Future<dynamic> insertDiscussionPost(
      String post, List<String> images, List<String> fileName) async {
    try {
      String uid = PreferenceService().userUid;
      final data = await HttpService().post(
          'insert_discussion_post.php',
          FormData.fromMap({
            'post': post,
            'user_id': uid,
            'fileName': fileName.isEmpty ? 'null' : fileName,
            'images': images.isEmpty ? 'null' : images,
          }));
      return jsonDecode(data.data);
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<List<DiscussionModel>> getDiscussionComment(int postId) async {
    try {
      final data = await HttpService().post(
          'get_discussion_comment.php', FormData.fromMap({'post_id': postId}));
      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => DiscussionModel.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<bool> insertDiscussionComment(String comment, int postId,
      List<String> images, List<String> fileName) async {
    try {
      String uid = PreferenceService().userUid;
      final data = await HttpService().post(
          'insert_discussion_comment.php',
          FormData.fromMap({
            'comment': comment,
            'user_id': uid,
            'post_id': postId,
            'fileName':
                fileName == null ? null : fileName.isEmpty ? null : fileName,
            'images': images == null ? null : images.isEmpty ? null : images
          }));

      return true;
    } catch (e) {
      print(e);

      return false;
    }
  }
}
