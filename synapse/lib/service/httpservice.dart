import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/service/preferenceservice.dart';

class HttpService {
  Future<dynamic> getService(String url) async {
    Dio _dio = Dio();
    _dio.options.baseUrl = baseUrl;
    try {
      Response response = await _dio.post(baseUrl + url);
      if (response.statusCode == 200) {
        return (response);
      }
      throw (response.statusMessage);
    } catch (e) {
      throw (e);
    }
  }

  Future<dynamic> post(String url, FormData formData) async {
    Dio _dio = Dio();
    _dio.options.baseUrl = baseUrl;
    try {
      Response response = await _dio.post(baseUrl + url, data: formData);
      if (response.statusCode == 200) {
        return (response);
      }
      throw (response.statusMessage);
    } catch (e) {
      throw (e);
    }
  }

  Future<String> downloadPicture(String url, String uid) async {
    if (url == null)
      return null;
    else if (url.isEmpty) return null;
    Dio _dio = Dio();
    Directory directory = await getApplicationDocumentsDirectory();

    String imgPath = directory.path + '/user/$uid.jpg';
    try {
      await _dio.download(url, imgPath);
      return imgPath;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
