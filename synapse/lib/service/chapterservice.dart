import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:synapse/model/chapter.dart';
import 'package:synapse/model/subject.dart';
import 'package:synapse/provider/chapterProvider.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

///dailytest=> topicId=0, subjectId=0
///mockTest=> topicId=-1,subjectId=-1
class ChapterService {
  Future<ChapterProvider> fetchTestChapters() async {
    final callback = await Future.wait([getDailyTest(), getMockTest()]);
    return ChapterProvider(dailyTest: callback[0], mockTest: callback[1]);
  }

  Future<List<Chapter>> getDailyTest() async {
    return await getChapterList(0, 0);
  }

  Future<List<Chapter>> getMockTest() async {
    return await getChapterList(-1, -1);
  }

  Future<List<Subject>> getSubjectbyId(int topicId) async {
    try {
      final data = await HttpService()
          .post('get_subject.php', FormData.fromMap({'topic_id': topicId}));
      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => Subject.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<Chapter>> getChapterList(int topicId, int subjectId) async {
    try {
      String userId = PreferenceService().userUid;
      final data = await HttpService().post(
          'get_chapter.php',
          FormData.fromMap({
            'topic_id': topicId,
            'subject_id': subjectId,
            'user_id': userId
          }));

      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => Chapter.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<Chapter>> getSuggestedTest() async {
    try {
      String userId = PreferenceService().userUid;
      final data = await HttpService().post(
          'get_suggested_test.php', FormData.fromMap({'user_id': userId}));

      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => Chapter.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<Chapter>> notificationCenter() async {
    try {
      String userId = PreferenceService().userUid;
      final data = await HttpService()
          .post('message_center.php', FormData.fromMap({'user_id': userId}));
      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => Chapter.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<bool> deleteTest(int chapterId) async {
    String userId = PreferenceService().userUid;
    try {
      await HttpService().post('delete_test_data.php',
          FormData.fromMap({'user_id': userId, 'chapter_id': chapterId}));
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
