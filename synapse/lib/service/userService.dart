import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:synapse/config/constant.dart';
import 'package:synapse/model/leaderboard.dart';
import 'package:synapse/model/user.dart';
import 'package:synapse/provider/leaderboardProvider.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

class UserService {
  Future<void> insertUserData(UserModel userModel) async {
    try {
      await HttpService().post(
        "insert_user.php",
        FormData.fromMap({
          "name": userModel.name ?? 'User',
          "provider": userModel.provider,
          "photo_url": userModel.photo_url,
          "email_id": userModel.email_id,
          "uid": userModel.firebase_uid,
        }),
      );
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateUserProfilePic(
    String photo,
  ) async {
    String uid = PreferenceService().userUid;
    try {
      await HttpService().post(
        "update_userpicture.php",
        FormData.fromMap({
          'uid': uid,
          'photo_url': photo,
        }),
      );
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateUserData(String name, String photoUrl) async {
    String uid = PreferenceService().userUid;
    try {
      await HttpService().post(
        "update_userdata.php",
        FormData.fromMap({
          'name': name,
          'uid': uid,
          'photo_url': photoUrl,
        }),
      );
    } catch (e) {
      print(e);
    }
  }

  Future<UserModel> getUserData(String uid) async {
    try {
      Response response = await HttpService().post(
        "get_userdata.php",
        FormData.fromMap({
          'uid': uid,
        }),
      );
      Map<String, dynamic> data = jsonDecode(response.data);
      if (data == null)
        return UserModel(name: null, photo_url: null, email_id: null);
      return UserModel(
          name: data['name'] ?? null,
          photo_url: data['photo_url'] ?? null,
          email_id: data['email_id'] ?? '');
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<LeaderBoardProvider> leaderBoardData() async {
    final temp = await Future.wait([
      getLeaderBoardData(0, 0),
      getLeaderBoardData(-1, -1),
      getLeaderBoardData(2, 2)
    ]);
    return LeaderBoardProvider(
        dailyTest: temp[0], mockTest: temp[1], subjectTest: temp[2]);
  }

  Future<List<LeaderBoardUserData>> getLeaderBoardData(
      int topicId, int subjectId) async {
    try {
      Response response = await HttpService().post(
        "leaderboard.php",
        FormData.fromMap({
          'topic_id': topicId,
          'subject_id': subjectId,
        }),
      );
      List<dynamic> data = jsonDecode(response.data);
      return data.map((e) => LeaderBoardUserData.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return [];
    }
  }
}
