import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:synapse/model/result.dart';
import 'package:synapse/model/resultAnswer.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

class ResultService {
  Future<Result> getChapterResult(int chapterId) async {
    try {
      String userId = PreferenceService().userUid;
      final data = await HttpService().post('generate_result.php',
          FormData.fromMap({'chapter_id': chapterId, 'user_id': userId}));
      dynamic response = jsonDecode(data.data);
      return Result.fromMap(response);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<ResultAnswer>> getResultAnswers(int chapterId) async {
    try {
      String userId = PreferenceService().userUid;
      final data = await HttpService().post('get_result_answer.php',
          FormData.fromMap({'chapter_id': chapterId, 'user_id': userId}));
      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => ResultAnswer.fromMap(e)).toList();
    } catch (e) {
      print(e);
      return null;
    }
  }
}
