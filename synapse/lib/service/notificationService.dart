import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class MessagingService {
  BuildContext context;
  void init(BuildContext context) {
    this.context = context;
    FirebaseMessaging _fireBaseMessaging = FirebaseMessaging();
    _fireBaseMessaging.subscribeToTopic("users");
    _fireBaseMessaging.configure(
      onMessage: (message) async {
        print(message);
        // String body = message['notification']['body'];
        // String title = message['notification']['title'];
        // String clickAction = message['data']['clickAction'];
        // int id = int.tryParse(message['data']['reference_id']) ?? 0;
      },
      onLaunch: (message) async {
        print(message);
        // String clickAction = (message['data']['clickAction']);
        // int id = (int.tryParse(message['data']['reference_id']) ?? 0);
      },
      onResume: (message) async {
        print(message);
        // String clickAction = (message['data']['clickAction']);
        // int id = (int.tryParse(message['data']['reference_id']) ?? 0);
      },
    );
    _fireBaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
        sound: true,
        badge: true,
        alert: true,
      ),
    );
    _fireBaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {});
  }
}
