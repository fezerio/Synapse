import 'package:synapse/model/leaderboard.dart';
import 'package:synapse/provider/leaderboardProvider.dart';

class LeaderBoardService {
  Future<List<LeaderBoardUserData>> getMoctTestLeaderBoard({int b}) async {
    await Future.delayed(Duration(seconds: 2));
    return [
      LeaderBoardUserData(
          name: 'Mock$b', right_answer: 20, wrong_answer: 30, total: 50),
      LeaderBoardUserData(
          name: 'Mock', right_answer: 20, wrong_answer: 30, total: 50),
      LeaderBoardUserData(
          name: 'Mock', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Mock', right_answer: 20, wrong_answer: 30, total: 20),
    ];
  }

  Future<List<LeaderBoardUserData>> getDailyTestLeaderBoard({int b}) async {
    await Future.delayed(Duration(seconds: 2));
    return [
      LeaderBoardUserData(
          name: 'Daily$b', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Daily', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Daily', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Daily', right_answer: 20, wrong_answer: 30, total: 20),
    ];
  }

  Future<List<LeaderBoardUserData>> getSujectTestLeaderBoard({int b}) async {
    await Future.delayed(Duration(seconds: 3));
    return [
      LeaderBoardUserData(
          name: 'Suject$b', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Suject', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Suject', right_answer: 20, wrong_answer: 30, total: 20),
      LeaderBoardUserData(
          name: 'Suject', right_answer: 20, wrong_answer: 30, total: 20),
    ];
  }

  Future<LeaderBoardProvider> getLeaderBoardData({int b}) async {
    final callback = await Future.wait([
      getMoctTestLeaderBoard(b: b),
      getDailyTestLeaderBoard(b: b),
      getSujectTestLeaderBoard(b: b)
    ]);
    return LeaderBoardProvider(
        mockTest: callback[0],
        dailyTest: callback[1],
        subjectTest: callback[2]);
  }
}
