import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:synapse/model/SummaryInfo.dart';
import 'package:synapse/model/bookmarkData.dart';
import 'package:synapse/service/httpservice.dart';
import 'package:synapse/service/preferenceservice.dart';

class BookmarkService {
  Future<List<BookMarkData>> getBookMarks() async {
    String userId = PreferenceService().userUid;
    try {
      final data = await HttpService()
          .post('get_bookmark.php', FormData.fromMap({'user_id': userId}));

      List<dynamic> response = jsonDecode(data.data);
      return response.map((e) => BookMarkData.fromMap(e)).toList();
    } catch (e) {
      return [];
    }
  }

  Future<bool> removeBookMark(int questionId) async {
    String userId = PreferenceService().userUid;
    try {
      final data = await HttpService().post(
          'remove_bookmark.php',
          FormData.fromMap({
            'user_id': userId,
            'question_id': questionId,
          }));

      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> reportQuestion(int questionId) async {
    String userId = PreferenceService().userUid;
    try {
      final data = await HttpService().post(
          'send_report_question.php',
          FormData.fromMap({
            'user_id': userId,
            'question_id': questionId,
          }));
      // print(data.data);
      // List<dynamic> response = jsonDecode(data.data);
      return true;
    } catch (e) {
      print(e);
      // return false;
    }
  }

  Future<SummaryInfo> getMcqTrackingData(String userId) async {
    try {
      final data = await HttpService().post(
          'get_mcqdata.php',
          FormData.fromMap({
            'user_id': userId,
          }));
      return SummaryInfo.fromMap(jsonDecode(data.data));
    } catch (e) {
      print(e);
      return null;
      // return false;
    }
  }

  Future<bool> insertBookmark(int questionId) async {
    String userId = PreferenceService().userUid;
    try {
      final data = await HttpService().post(
          'insert_bookmark.php',
          FormData.fromMap({
            'user_id': userId,
            'question_id': questionId,
          }));
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
