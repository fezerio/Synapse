import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceService {
  static PreferenceService _instance;
  static SharedPreferences _prefs;

  static ValueNotifier<bool> isReady = ValueNotifier(false);

  PreferenceService._() {
    SharedPreferences.getInstance().then(
      (prefs) {
        _prefs = prefs;
        isReady.value = true;
      },
    );
  }

  factory PreferenceService() {
    _instance ??= PreferenceService._();
    return _instance;
  }

  /// uid from firebase
  set userUid(String uid) => _prefs.setString('userUid', uid);
  String get userUid => _prefs.getString('userUid') ?? null;

//user's name
  set displayName(String uid) => _prefs.setString('displayName', uid);
  String get displayName => _prefs.getString('displayName') ?? "FName LName";

  ///users pics local url
  set photoUrl(String uid) => _prefs.setString('photoUrl', uid);
  String get photoUrl => _prefs.getString('photoUrl') ?? '';

  set emailId(String emailId) => _prefs.setString('emailId', emailId);
  String get emailId => _prefs.getString('emailId') ?? null;

  ///fb/google/firebase-email
  set provider(String provider) => _prefs.setString('provider', provider);
  String get provider => _prefs.getString('provider') ?? 'firebase';

  // set mcqRightAnswer(int count) => _prefs.setInt(
  //     'mcqRightAnswer', count == null ? mcqRightAnswer + 1 : count);
  // int get mcqRightAnswer => _prefs.getInt('mcqRightAnswer') ?? 0;

  // set mcqWrongAnswer(int count) => _prefs.setInt(
  //     'mcqWrongAnswer', count == null ? mcqWrongAnswer + 1 : count);
  // int get mcqWrongAnswer => _prefs.getInt('mcqWrongAnswer') ?? 0;

  // set totalTestGiven(int count) => _prefs.setInt(
  //     'totalTestGiven', count == null ? totalTestGiven + 1 : count);
  // int get totalTestGiven => _prefs.getInt('totalTestGiven') ?? 0;

  Future<bool> clearPreferences() async {
    bool callback;
    try {
      callback = await File(photoUrl).exists();
    } catch (e) {
      print(e);
    }
    if (callback ?? false) {
      try {
        await File(photoUrl).delete();
      } catch (e) {
        print(e);
      }
    }
    return await _prefs.clear();
  }
}
