import 'dart:convert';

class LeaderBoardUserData {
  final String photo_url;
  final String name;
  final String user_id;
  final int total;
  final int right_answer;
  final int wrong_answer;
  LeaderBoardUserData({
    this.photo_url,
    this.name,
    this.user_id,
    this.total,
    this.right_answer,
    this.wrong_answer,
  });

  LeaderBoardUserData copyWith({
    String photo_url,
    String name,
    String user_id,
    int total,
    int right_answer,
    int wrong_answer,
  }) {
    return LeaderBoardUserData(
      photo_url: photo_url ?? this.photo_url,
      name: name ?? this.name,
      user_id: user_id ?? this.user_id,
      total: total ?? this.total,
      right_answer: right_answer ?? this.right_answer,
      wrong_answer: wrong_answer ?? this.wrong_answer,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'photo_url': photo_url,
      'name': name,
      'user_id': user_id,
      'total': total,
      'right_answer': right_answer,
      'wrong_answer': wrong_answer,
    };
  }

  factory LeaderBoardUserData.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return LeaderBoardUserData(
      photo_url: map['photo_url'],
      name: map['name'],
      user_id: map['user_id'],
      total: int.tryParse(map['total'].toString()) ?? 0,
      right_answer: int.tryParse(map['right_answer'].toString()) ?? 0,
      wrong_answer: int.tryParse(map['wrong_answer'].toString()) ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory LeaderBoardUserData.fromJson(String source) =>
      LeaderBoardUserData.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LeaderBoardUserData(photo_url: $photo_url, name: $name, user_id: $user_id, total: $total, right_answer: $right_answer, wrong_answer: $wrong_answer)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is LeaderBoardUserData &&
        o.photo_url == photo_url &&
        o.name == name &&
        o.user_id == user_id &&
        o.total == total &&
        o.right_answer == right_answer &&
        o.wrong_answer == wrong_answer;
  }

  @override
  int get hashCode {
    return photo_url.hashCode ^
        name.hashCode ^
        user_id.hashCode ^
        total.hashCode ^
        right_answer.hashCode ^
        wrong_answer.hashCode;
  }
}
