/// code->1 = sign_in error
///code ->0 = default exception handling
class ErrorHandler {
  final int code;
  final String message;

  ErrorHandler({this.code, this.message});
}
