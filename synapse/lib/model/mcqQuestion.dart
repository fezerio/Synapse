import 'dart:convert';

import 'package:synapse/model/question.dart';

class MCQQuestion {
  final Question question;
  final bool isSolved;
  final int userAnswer;
  MCQQuestion({this.question, this.isSolved, this.userAnswer});

  MCQQuestion copyWith({
    Question question,
    bool isSolved,
    int userAnswer,
  }) {
    return MCQQuestion(
      question: question ?? this.question,
      isSolved: isSolved ?? this.isSolved,
      userAnswer: userAnswer ?? this.userAnswer,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'question': question?.toMap(),
      'isSolved': isSolved,
      'userAnswer': userAnswer,
    };
  }

  factory MCQQuestion.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return MCQQuestion(
      question: Question.fromMap(map['question']),
      isSolved: map['isSolved'].toString() == '1',
      userAnswer: int.tryParse((map['userAnswer'] ?? -1).toString()) ?? -1,
    );
  }

  String toJson() => json.encode(toMap());

  factory MCQQuestion.fromJson(String source) =>
      MCQQuestion.fromMap(json.decode(source));

  @override
  String toString() =>
      'MCQQuestion(question: $question, isSolved: $isSolved, userAnswer: $userAnswer)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is MCQQuestion &&
        o.question == question &&
        o.isSolved == isSolved &&
        o.userAnswer == userAnswer;
  }

  @override
  int get hashCode =>
      question.hashCode ^ isSolved.hashCode ^ userAnswer.hashCode;
}
