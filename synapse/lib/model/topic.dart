import 'dart:convert';

class Topic {
  final int id;
  final String name;
  Topic({
    this.id,
    this.name,
  });

  Topic copyWith({
    int id,
    String name,
  }) {
    return Topic(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  factory Topic.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Topic(
      id: int.tryParse(map['id'].toString()) ?? 0,
      name: map['name'],
    );
  }

  factory Topic.fromJson(String source) => Topic.fromMap(json.decode(source));

  @override
  String toString() => 'Topic(id: $id, name: $name)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Topic && o.id == id && o.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
