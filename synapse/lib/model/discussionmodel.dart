import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:synapse/model/discussionpost.dart';

class DiscussionModel {
  final DiscussionPost discussionPost;
  final List<String> attachments;
  DiscussionModel({
    this.discussionPost,
    this.attachments,
  });

  DiscussionModel copyWith({
    DiscussionPost discussionPost,
    List<String> attachments,
  }) {
    return DiscussionModel(
      discussionPost: discussionPost ?? this.discussionPost,
      attachments: attachments ?? this.attachments,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'discussionPost': discussionPost?.toMap(),
      'attachments': attachments,
    };
  }

  factory DiscussionModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return DiscussionModel(
      discussionPost: DiscussionPost.fromMap(map['discussionPost']),
      attachments: List<String>.from(map['attachments']),
    );
  }

  String toJson() => json.encode(toMap());

  factory DiscussionModel.fromJson(String source) =>
      DiscussionModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'DiscussionModel(discussionPost: $discussionPost, attachments: $attachments)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DiscussionModel &&
        o.discussionPost == discussionPost &&
        listEquals(o.attachments, attachments);
  }

  @override
  int get hashCode => discussionPost.hashCode ^ attachments.hashCode;
}
