class SummaryInfo {
  final int testGiven;
  final int mcqRightAnswer;
  final int mcqWrongAnswer;
  final int total_attempted;
  SummaryInfo({
    this.testGiven,
    this.mcqRightAnswer,
    this.mcqWrongAnswer,
    this.total_attempted,
  });

  factory SummaryInfo.fromMap(Map<String, dynamic> mcqData) {
    if (mcqData == null) return null;

    return SummaryInfo(
      testGiven:
          int.tryParse((mcqData['total_test_given'] ?? 0).toString()) ?? 0,
      mcqRightAnswer:
          int.tryParse((mcqData['rightAnswer'] ?? 0).toString()) ?? 0,
      mcqWrongAnswer:
          int.tryParse((mcqData['wrongAnswer'] ?? 0).toString()) ?? 0,
      total_attempted:
          int.tryParse((mcqData['total_attempted'] ?? 0).toString()) ?? 0,
    );
  }

  @override
  String toString() =>
      'SummaryInfo(testGiven: $testGiven, mcqRightAnswer: $mcqRightAnswer, mcqWrongAnswer: $mcqWrongAnswer)';
}
