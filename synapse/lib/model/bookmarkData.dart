import 'dart:convert';

import 'package:synapse/model/bookmark.dart';
import 'package:synapse/model/question.dart';

class BookMarkData {
  final Bookmark bookmarks;
  final Question question;
  BookMarkData({
    this.bookmarks,
    this.question,
  });

  BookMarkData copyWith({
    Bookmark bookmarks,
    Question question,
  }) {
    return BookMarkData(
      bookmarks: bookmarks ?? this.bookmarks,
      question: question ?? this.question,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'bookmarks': bookmarks?.toMap(),
      'question': question?.toMap(),
    };
  }

  factory BookMarkData.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return BookMarkData(
      bookmarks: Bookmark.fromMap(map['bookmarks']),
      question: Question.fromMap(map['question']),
    );
  }

  String toJson() => json.encode(toMap());

  factory BookMarkData.fromJson(String source) =>
      BookMarkData.fromMap(json.decode(source));

  @override
  String toString() =>
      'BookMarkData(bookmarks: $bookmarks, question: $question)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is BookMarkData &&
        o.bookmarks == bookmarks &&
        o.question == question;
  }

  @override
  int get hashCode => bookmarks.hashCode ^ question.hashCode;
}
