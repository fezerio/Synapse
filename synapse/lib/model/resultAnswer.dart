import 'dart:convert';

class ResultAnswer {
  final int id;
  final int chapter_id;
  final String question;
  final String option_a;
  final String option_b;
  final String option_c;
  final String option_d;
  // final int chapter_id;
  final String explanation;
  final int answer;
  final int user_answer;
  final int question_tracking_id;

  ResultAnswer({
    this.id,
    this.chapter_id,
    this.question,
    this.option_a,
    this.option_b,
    this.option_c,
    this.option_d,
    this.explanation,
    this.answer,
    this.user_answer,
    this.question_tracking_id,
  });

  ResultAnswer copyWith({
    int id,
    int chapter_id,
    String question,
    String option_a,
    String option_b,
    String option_c,
    String option_d,
    String explanation,
    int answer,
    int user_answer,
    int question_tracking_id,
  }) {
    return ResultAnswer(
      id: id ?? this.id,
      chapter_id: chapter_id ?? this.chapter_id,
      question: question ?? this.question,
      option_a: option_a ?? this.option_a,
      option_b: option_b ?? this.option_b,
      option_c: option_c ?? this.option_c,
      option_d: option_d ?? this.option_d,
      explanation: explanation ?? this.explanation,
      answer: answer ?? this.answer,
      user_answer: user_answer ?? this.user_answer,
      question_tracking_id: question_tracking_id ?? this.question_tracking_id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'chapter_id': chapter_id,
      'question': question,
      'option_a': option_a,
      'option_b': option_b,
      'option_c': option_c,
      'option_d': option_d,
      'explanation': explanation,
      'answer': answer,
      'user_answer': user_answer,
      'question_tracking_id': question_tracking_id,
    };
  }

  factory ResultAnswer.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return ResultAnswer(
      id: int.tryParse(map['id']) ?? 0,
      chapter_id: int.tryParse(map['chapter_id']) ?? 0,
      question: map['question'],
      option_a: map['option_a'],
      option_b: map['option_b'],
      option_c: map['option_c'],
      option_d: map['option_d'],
      explanation: map['explanation'],
      answer: int.tryParse(map['answer']) ?? 0,
      user_answer: int.tryParse((map['user_answer'] ?? -1).toString()) ?? 0,
      question_tracking_id:
          int.tryParse((map['question_tracking_id'] ?? -1).toString()) ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory ResultAnswer.fromJson(String source) =>
      ResultAnswer.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ResultAnswer(id: $id, chapter_id: $chapter_id, question: $question, option_a: $option_a, option_b: $option_b, option_c: $option_c, option_d: $option_d, explanation: $explanation, answer: $answer, user_answer: $user_answer, question_tracking_id: $question_tracking_id)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ResultAnswer &&
        o.id == id &&
        o.chapter_id == chapter_id &&
        o.question == question &&
        o.option_a == option_a &&
        o.option_b == option_b &&
        o.option_c == option_c &&
        o.option_d == option_d &&
        o.explanation == explanation &&
        o.answer == answer &&
        o.user_answer == user_answer &&
        o.question_tracking_id == question_tracking_id;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        chapter_id.hashCode ^
        question.hashCode ^
        option_a.hashCode ^
        option_b.hashCode ^
        option_c.hashCode ^
        option_d.hashCode ^
        explanation.hashCode ^
        answer.hashCode ^
        user_answer.hashCode ^
        question_tracking_id.hashCode;
  }
}
