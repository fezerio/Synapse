import 'dart:convert';

class Result {
  final int id;
  final String user_id;
  final int chapter_id;
  final int attempted_question;
  final int left_question;
  final int correct_answer;
  final int incorrect_answer;
  final String created_date;
  Result({
    this.id,
    this.user_id,
    this.chapter_id,
    this.attempted_question,
    this.left_question,
    this.correct_answer,
    this.incorrect_answer,
    this.created_date,
  });

  Result copyWith({
    int id,
    String user_id,
    int chapter_id,
    int attempted_question,
    int left_question,
    int correct_answer,
    int incorrect_answer,
    String created_date,
  }) {
    return Result(
      id: id ?? this.id,
      user_id: user_id ?? this.user_id,
      chapter_id: chapter_id ?? this.chapter_id,
      attempted_question: attempted_question ?? this.attempted_question,
      left_question: left_question ?? this.left_question,
      correct_answer: correct_answer ?? this.correct_answer,
      incorrect_answer: incorrect_answer ?? this.incorrect_answer,
      created_date: created_date ?? this.created_date,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'user_id': user_id,
      'chapter_id': chapter_id,
      'attempted_question': attempted_question,
      'left_question': left_question,
      'correct_answer': correct_answer,
      'incorrect_answer': incorrect_answer,
      'created_date': created_date,
    };
  }

  factory Result.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Result(
      id: int.tryParse(map['id']) ?? 0,
      user_id: map['user_id'],
      chapter_id: int.tryParse(map['chapter_id']) ?? 0,
      attempted_question: int.tryParse(map['attempted_question']) ?? 0,
      left_question: int.tryParse(map['left_question']) ?? 0,
      correct_answer: int.tryParse(map['correct_answer']) ?? 0,
      incorrect_answer: int.tryParse(map['incorrect_answer']) ?? 0,
      created_date: map['created_date'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Result.fromJson(String source) => Result.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Result(id: $id, user_id: $user_id, chapter_id: $chapter_id, attempted_question: $attempted_question, left_question: $left_question, correct_answer: $correct_answer, incorrect_answer: $incorrect_answer, created_date: $created_date)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Result &&
        o.id == id &&
        o.user_id == user_id &&
        o.chapter_id == chapter_id &&
        o.attempted_question == attempted_question &&
        o.left_question == left_question &&
        o.correct_answer == correct_answer &&
        o.incorrect_answer == incorrect_answer &&
        o.created_date == created_date;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        user_id.hashCode ^
        chapter_id.hashCode ^
        attempted_question.hashCode ^
        left_question.hashCode ^
        correct_answer.hashCode ^
        incorrect_answer.hashCode ^
        created_date.hashCode;
  }
}
