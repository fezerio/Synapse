import 'dart:convert';

import 'package:synapse/model/question.dart';

class Bookmark {
  final int id;
  final String user_id;
  final int question_id;
  final String created_date;
  // final Question question;
  Bookmark({
    this.id,
    this.user_id,
    this.question_id,
    this.created_date,
    // this.question,
  });

  Bookmark copyWith({
    int id,
    String user_id,
    int question_id,
    String created_date,
    Question question,
  }) {
    return Bookmark(
      id: id ?? this.id,
      user_id: user_id ?? this.user_id,
      question_id: question_id ?? this.question_id,
      created_date: created_date ?? this.created_date,
      // question: question ?? this.question,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'user_id': user_id,
      'question_id': question_id,
      'created_date': created_date,
      // 'question': question?.toMap(),
    };
  }

  factory Bookmark.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Bookmark(
      id: int.tryParse(map['id'].toString()) ?? 0,
      user_id: map['user_id'],
      question_id: int.tryParse(map['question_id'].toString()) ?? 0,
      created_date: map['created_date'],
      // question: Question.fromMap(map['question']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Bookmark.fromJson(String source) =>
      Bookmark.fromMap(json.decode(source));

  // @override
  // String toString() {
  //   return 'Bookmark(id: $id, user_id: $user_id, question_id: $question_id, created_date: $created_date, question: $question)';
  // }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Bookmark &&
        o.id == id &&
        o.user_id == user_id &&
        o.question_id == question_id &&
        o.created_date == created_date;
    // o.question == question;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        user_id.hashCode ^
        question_id.hashCode ^
        created_date.hashCode;
    // question.hashCode;
  }
}
