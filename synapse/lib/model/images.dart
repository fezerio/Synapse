import 'dart:convert';

class Images {
  final int id;
  final int question_id;
  final String image;
  Images({
    this.id,
    this.question_id,
    this.image,
  });

  Images copyWith({
    int id,
    int question_id,
    String image,
  }) {
    return Images(
      id: id ?? this.id,
      question_id: question_id ?? this.question_id,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'question_id': question_id,
      'image': image,
    };
  }

  factory Images.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Images(
      id: int.tryParse(map['id'].toString()) ?? 0,
      question_id: int.tryParse(map['question_id'].toString()) ?? 0,
      image: map['image'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Images.fromJson(String source) => Images.fromMap(json.decode(source));

  @override
  String toString() =>
      'Images(id: $id, question_id: $question_id, image: $image)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Images &&
        o.id == id &&
        o.question_id == question_id &&
        o.image == image;
  }

  @override
  int get hashCode => id.hashCode ^ question_id.hashCode ^ image.hashCode;
}
