import 'dart:convert';

class DiscussionPost {
  final int id;
  final String user_id;
  final String created_date;
  final String post;
  final String created_user_name;
  final String created_user_id;
  final String created_user_image;
  final bool islocal;
  DiscussionPost({
    this.id,
    this.user_id,
    this.islocal,
    this.created_date,
    this.post,
    this.created_user_name,
    this.created_user_id,
    this.created_user_image,
  });

  DiscussionPost copyWith({
    int id,
    String user_id,
    String created_date,
    String post,
    String created_user_name,
    String created_user_id,
    String created_user_image,
  }) {
    return DiscussionPost(
      id: id ?? this.id,
      user_id: user_id ?? this.user_id,
      created_date: created_date ?? this.created_date,
      post: post ?? this.post,
      created_user_name: created_user_name ?? this.created_user_name,
      created_user_id: created_user_id ?? this.created_user_id,
      created_user_image: created_user_image ?? this.created_user_image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'user_id': user_id,
      'created_date': created_date,
      'post': post,
      'created_user_name': created_user_name,
      'created_user_id': created_user_id,
      'created_user_image': created_user_image,
    };
  }

  factory DiscussionPost.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return DiscussionPost(
      id: int.tryParse((map['id'] ?? 0).toString()) ?? 0,
      user_id: map['user_id'],
      created_date: map['created_date'],
      post: map['post'],
      created_user_name: map['created_user_name'],
      created_user_id: map['created_user_id'],
      created_user_image: map['created_user_image'],
    );
  }

  String toJson() => json.encode(toMap());

  factory DiscussionPost.fromJson(String source) =>
      DiscussionPost.fromMap(json.decode(source));

  @override
  String toString() {
    return 'DiscussionPost(id: $id, user_id: $user_id, created_date: $created_date, post: $post, created_user_name: $created_user_name, created_user_id: $created_user_id, created_user_image: $created_user_image)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is DiscussionPost &&
        o.id == id &&
        o.user_id == user_id &&
        o.created_date == created_date &&
        o.post == post &&
        o.created_user_name == created_user_name &&
        o.created_user_id == created_user_id &&
        o.created_user_image == created_user_image;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        user_id.hashCode ^
        created_date.hashCode ^
        post.hashCode ^
        created_user_name.hashCode ^
        created_user_id.hashCode ^
        created_user_image.hashCode;
  }
}
