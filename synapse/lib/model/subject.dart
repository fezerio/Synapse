import 'dart:convert';

class Subject {
  final int id;
  final String name;
  final int topic_id;
  final String photourl;
  Subject({
    this.id,
    this.name,
    this.topic_id,
    this.photourl,
  });

  Subject copyWith({
    int id,
    String name,
    int topic_id,
    String photourl,
  }) {
    return Subject(
      id: id ?? this.id,
      name: name ?? this.name,
      topic_id: topic_id ?? this.topic_id,
      photourl: photourl ?? this.photourl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'topic_id': topic_id,
      'photourl': photourl,
    };
  }

  factory Subject.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Subject(
      id: int.tryParse(map['id'].toString()) ?? 0,
      name: map['name'],
      photourl: map['photourl'],
      topic_id: int.tryParse(map['topic_id'].toString()) ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Subject.fromJson(String source) =>
      Subject.fromMap(json.decode(source));

  @override
  String toString() => 'Subject(id: $id, name: $name, topic_id: $topic_id)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Subject &&
        o.id == id &&
        o.name == name &&
        o.topic_id == topic_id;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ topic_id.hashCode;
}
