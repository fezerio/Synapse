import 'dart:convert';

class UserModel {
  final int id;
  final String name;
  final String provider;
  final String photo_url;
  final String firebase_uid;
  final String email_id;
  UserModel({
    this.id,
    this.name,
    this.provider,
    this.photo_url,
    this.firebase_uid,
    this.email_id,
  });

  UserModel copyWith({
    int id,
    String name,
    String provider,
    String photo_url,
    String firebase_uid,
    String email_id,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      provider: provider ?? this.provider,
      photo_url: photo_url ?? this.photo_url,
      firebase_uid: firebase_uid ?? this.firebase_uid,
      email_id: email_id ?? this.email_id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'provider': provider,
      'photo_url': photo_url,
      'firebase_uid': firebase_uid,
      'email_id': email_id,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return UserModel(
      id: int.tryParse(map['id'].toString()) ?? 0,
      name: map['name'],
      provider: map['provider'],
      photo_url: map['photo_url'],
      firebase_uid: map['firebase_uid'],
      email_id: map['email_id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserModel(id: $id, name: $name, provider: $provider, photo_url: $photo_url, firebase_uid: $firebase_uid, email_id: $email_id)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UserModel &&
        o.id == id &&
        o.name == name &&
        o.provider == provider &&
        o.photo_url == photo_url &&
        o.firebase_uid == firebase_uid &&
        o.email_id == email_id;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        provider.hashCode ^
        photo_url.hashCode ^
        firebase_uid.hashCode ^
        email_id.hashCode;
  }
}
