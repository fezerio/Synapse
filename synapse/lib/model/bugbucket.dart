import 'dart:convert';

class BugBucket {
  final int id;
  final String explanation;
  final String question;
  final String optionA;
  final String optionB;
  final String optionC;
  final String optionD;
  final int question_id;
  final int chapter_id;
  BugBucket({
    this.id,
    this.explanation,
    this.question,
    this.optionA,
    this.optionB,
    this.optionC,
    this.optionD,
    this.question_id,
    this.chapter_id,
  });

  BugBucket copyWith({
    int id,
    String explanation,
    String question,
    String optionA,
    String optionB,
    String optionC,
    String optionD,
    int question_id,
    int chapter_id,
  }) {
    return BugBucket(
      id: id ?? this.id,
      explanation: explanation ?? this.explanation,
      question: question ?? this.question,
      optionA: optionA ?? this.optionA,
      optionB: optionB ?? this.optionB,
      optionC: optionC ?? this.optionC,
      optionD: optionD ?? this.optionD,
      question_id: question_id ?? this.question_id,
      chapter_id: chapter_id ?? this.chapter_id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'explanation': explanation,
      'question': question,
      'optionA': optionA,
      'optionB': optionB,
      'optionC': optionC,
      'optionD': optionD,
      'question_id': question_id,
      'chapter_id': chapter_id,
    };
  }

  factory BugBucket.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return BugBucket(
      id: map['id'],
      explanation: map['explanation'],
      question: map['question'],
      optionA: map['optionA'],
      optionB: map['optionB'],
      optionC: map['optionC'],
      optionD: map['optionD'],
      question_id: map['question_id'],
      chapter_id: map['chapter_id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory BugBucket.fromJson(String source) =>
      BugBucket.fromMap(json.decode(source));

  @override
  String toString() {
    return 'BugBucket(id: $id, explanation: $explanation, question: $question, optionA: $optionA, optionB: $optionB, optionC: $optionC, optionD: $optionD, question_id: $question_id, chapter_id: $chapter_id)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is BugBucket &&
        o.id == id &&
        o.explanation == explanation &&
        o.question == question &&
        o.optionA == optionA &&
        o.optionB == optionB &&
        o.optionC == optionC &&
        o.optionD == optionD &&
        o.question_id == question_id &&
        o.chapter_id == chapter_id;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        explanation.hashCode ^
        question.hashCode ^
        optionA.hashCode ^
        optionB.hashCode ^
        optionC.hashCode ^
        optionD.hashCode ^
        question_id.hashCode ^
        chapter_id.hashCode;
  }
}
