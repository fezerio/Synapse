import 'dart:convert';

class Question {
  final int id;
  final String question;
  final String option_a;
  final String option_b;
  final String option_c;
  final String option_d;
  final int chapter_id;
  final String explanation;
  final String chapter_name;
  final int answer;
  Question({
    this.id,
    this.question,
    this.option_a,
    this.option_b,
    this.option_c,
    this.option_d,
    this.chapter_name,
    this.chapter_id,
    this.explanation,
    this.answer,
  });

  Question copyWith({
    int id,
    String question,
    String optionA,
    String optionB,
    String optionC,
    String optionD,
    int chapter_id,
    String explanation,
    String chapter_name,
    int answer,
  }) {
    return Question(
      id: id ?? this.id,
      question: question ?? this.question,
      option_a: optionA ?? this.option_a,
      option_b: optionB ?? this.option_b,
      option_c: optionC ?? this.option_c,
      option_d: optionD ?? this.option_d,
      chapter_name: chapter_name ?? this.chapter_name,
      chapter_id: chapter_id ?? this.chapter_id,
      explanation: explanation ?? this.explanation,
      answer: answer ?? this.answer,
    );
  }

  factory Question.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Question(
      id: int.tryParse(map['id'].toString()) ?? 0,
      question: map['question'],
      option_a: map['option_a'],
      option_b: map['option_b'],
      option_c: map['option_c'],
      option_d: map['option_d'],
      chapter_name: map['chapter_name'],
      chapter_id: int.tryParse(map['chapter_id'].toString()) ?? 0,
      explanation: map['explanation'],
      answer: int.tryParse(map['answer'].toString()) ?? 0,
    );
  }

  // factory Question.getAnswerValue(
  //     String a, String b, String c, String d, int answerIndex) {
  //   String answer;
  //   switch (answerIndex) {
  //     case 1:
  //       answer = a;
  //       break;
  //     case 2:
  //       answer = b;
  //       break;
  //     case 3:
  //       answer = c;
  //       break;
  //     case 4:
  //       answer = d;
  //       break;
  //     default:
  //       answer = '';
  //   }
  //   return Question(answer: answer);
  // }

  factory Question.fromJson(String source) =>
      Question.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Question(id: $id, question: $question, optionA: $option_a, optionB: $option_b, optionC: $option_c, optionD: $option_d, chapter_id: $chapter_id, explanation: $explanation, answer: $answer)';
  }

  // @override
  // bool operator ==(Object o) {
  //   if (identical(this, o)) return true;

  //   return o is Question &&
  //       o.id == id &&
  //       o.question == question &&
  //       o.optionA == optionA &&
  //       o.optionB == optionB &&
  //       o.optionC == optionC &&
  //       o.optionD == optionD &&
  //       o.chapter_id == chapter_id &&
  //       o.explanation == explanation &&
  //       o.answer == answer;
  // }

  // @override
  // int get hashCode {
  //   return id.hashCode ^
  //       question.hashCode ^
  //       optionA.hashCode ^
  //       optionB.hashCode ^
  //       optionC.hashCode ^
  //       optionD.hashCode ^
  //       chapter_id.hashCode ^
  //       explanation.hashCode ^
  //       answer.hashCode;
  // }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'question': question,
      'optionA': option_a,
      'optionB': option_b,
      'optionC': option_c,
      'optionD': option_d,
      'chapter_id': chapter_id,
      'explanation': explanation,
      'answer': answer,
      'chapter_name': chapter_name,
    };
  }

  String toJson() => json.encode(toMap());
}
