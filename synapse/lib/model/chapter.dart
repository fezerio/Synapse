import 'dart:convert';

class Chapter {
  final int id;
  final String name;
  final int topic_id;
  final int subject_id;
  final String created_date;
  final String live_date;
  final int no_of_questions;
  final int timer;
  final bool completed;
  final bool not_completed;
  Chapter({
    this.id,
    this.name,
    this.live_date,
    this.topic_id,
    this.subject_id,
    this.created_date,
    this.no_of_questions,
    this.completed,
    this.not_completed,
    this.timer,
  });

  Chapter copyWith({
    int id,
    String name,
    int topic_id,
    int subject_id,
    int created_date,
    int no_of_questions,
    int completed,
    String live_date,
    int not_completed,
    int timer,
  }) {
    return Chapter(
      id: id ?? this.id,
      name: name ?? this.name,
      topic_id: topic_id ?? this.topic_id,
      subject_id: subject_id ?? this.subject_id,
      created_date: created_date ?? this.created_date,
      no_of_questions: no_of_questions ?? this.no_of_questions,
      completed: completed ?? this.completed,
      not_completed: not_completed ?? this.not_completed,
      timer: timer ?? this.timer,
      live_date: live_date ?? this.live_date,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'topic_id': topic_id,
      'subject_id': subject_id,
      'created_date': created_date,
      'completed': completed,
      'not_completed': not_completed,
      'no_of_questions': no_of_questions,
      'timer': timer,
      'live_date': live_date,
    };
  }

  factory Chapter.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Chapter(
      id: int.tryParse(map['id'].toString()) ?? 0,
      name: map['name'],
      topic_id: int.tryParse(map['topic_id'].toString()) ?? 0,
      subject_id: int.tryParse(map['subject_id'].toString()) ?? 0,
      created_date: map['created_date'],
      live_date: map['live_date'],
      no_of_questions: int.tryParse(map['no_of_questions'].toString()) ?? 0,
      completed: (int.tryParse(map['completed'].toString()) ?? 0) == 1,
      not_completed: (int.tryParse(map['not_completed'].toString()) ?? 0) == 1,
      timer: int.tryParse(map['timer'].toString()) ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Chapter.fromJson(String source) =>
      Chapter.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Chapter(id: $id, name: $name, topic_id: $topic_id, subject_id: $subject_id, created_date: $created_date, no_of_questions: $no_of_questions, timer: $timer)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Chapter &&
        o.id == id &&
        o.name == name &&
        o.topic_id == topic_id &&
        o.subject_id == subject_id &&
        o.created_date == created_date &&
        o.no_of_questions == no_of_questions &&
        o.timer == timer;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        topic_id.hashCode ^
        subject_id.hashCode ^
        created_date.hashCode ^
        no_of_questions.hashCode ^
        timer.hashCode;
  }
}
